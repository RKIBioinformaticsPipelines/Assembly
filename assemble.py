#!/usr/bin/python3
import argparse
import os
import yaml
import re
import tools
import subprocess
import sys
import datetime
import logging

__author__ = "Berit Haldemann"
__version__ = "0.0.4"
__status__ = "Development"

def main():
   
   parser = argparse.ArgumentParser(description = "Assemble sequencing reads to high quality contigs.")
     
   # Input

   groupInput = parser.add_argument_group("Input")    
   groupInput.add_argument("-i", "--input", help = "Either the original illumina data folder or trimmed output folder of QC pipeline. Please note: files must follow naming convention of Illumina or QC pipeline, respectively.", type = str, action = "store")
   groupInput.add_argument("-1", "--read1", help = "Read 1 in paired sequencing reads, if multiple files (separated by ,) are provided: order of files must match to read 2.", type = str, action = "store")
   groupInput.add_argument("-2", "--read2", help = "Read 2 in paired sequencing reads, if multiple files separated by ,) are provided: order of files must match to read 1.", type = str, action = "store")
   groupInput.add_argument("-u", "--unpaired", help = "Unpaired sequencing reads separated by ,.", type = str, action = "store")
   groupInput.add_argument("-m", "--sampleNamesMap", help = "TSV File with two columns: <old sample name> <new_sample name>.", type = str)
   groupInput.add_argument("-s", "--sampleName", help = "Sample name if single files are provided (Default: singleSample).", type = str, action = "store", default = "singleSample")
   groupInput.add_argument("-c", "--config", help = "Config file providing parameter settings. Parameters given via command line will override config file settings. (Currently not supported)", type = str, action = "store")
   
   # Output
  
   groupOutput = parser.add_argument_group("Output")  
   groupOutput.add_argument("-o", "--output", help = "Path to output folder. (Default: " + os.getcwd() + ")", type = str, default = os.getcwd())
   groupOutput.add_argument("-l", "--log", help = "Path to folder for log file. (Default: " + os.getcwd() + ")", type = str, default = os.getcwd())

   # parameter RAMBO-K

   groupRambok = parser.add_argument_group("Background read removal (optional)") 
   groupRambok.add_argument("--rambok_fg", help = "Reference file of foreground species in fasta-format used for RAMBO-K.", type = str, default = "")
   groupRambok.add_argument("--rambok_bg", help = "Reference file of background (host) species used for RAMBO-K.", type = str, default = "")
   groupRambok.add_argument("--rambok_fg_name", help = "Name of foreground species used for RAMBO-K. (Default: species1)", type = str, default = "species1")
   groupRambok.add_argument("--rambok_params", help = "Additional parameters used for RAMBO-K. (Default: kmer = 8, cutoff = 0). Please note: RAMBOK parameter settings for -r/--reffile1 and -R/--reffile2 will be overwritten by specified values given with --rambok_fg and --rambok_bg!", type = str, default = "")
   groupRambok.add_argument("--rambok_k", help = "Specific kmer size used for RAMBO-K.", type = str, default = "")
   groupRambok.add_argument("--rambok_cutoff", help = "Cutoff used for RAMBO-K. Use m1 for -1 (m for minus).", type = str, default = "") 
   groupRambok.add_argument("--rambok_test", help = "Run RAMBO-K in test modus to evaluate different parameter settings. If selected, kmers from 4 to 12 will be tested per default, but can be changed with --rambok_params.", action = "store_true" )

   # Subsampling

   groupSubsampling= parser.add_argument_group("Subsampling") 
   groupSubsampling.add_argument("--coverage", help = "Coverage for subsampling. (Default: 100)", type = str, default = "100")
   groupSubsampling.add_argument("--no_subsampling", help = "Switch off subsampling before assembly if expected coverage is too high.", action = "store_true")

   # Assembly
   
   groupAssembly = parser.add_argument_group("Assembly") 
   groupAssembly.add_argument("--assemblytools", help = "Assembly tools that should be used during assembly step. Choose from velvet, spades or a5 or any combination. (Default: velvet,spades)", type = str, default = "velvet,spades")
   groupAssembly.add_argument("--velvetopt_hash_lengths", help = "Hash lengths to be tested by the Velvet Optimiser in format start,end,step.", type = str, default = "")
   groupAssembly.add_argument("--velvetopt_params", help = "Additional parameters used for the Velvet Optimiser.", type = str, default = "")
   groupAssembly.add_argument("--velveth_params", help = "Additional parameters used for velveth (included in Velvet Optimiser).", type = str, default = "")
   groupAssembly.add_argument("--velvetg_params", help = "Additional parameters used for velvetg (included in Velvet Optimiser).", type = str, default = "")
   groupAssembly.add_argument("--spades_params", help = "Additional parameters used for SPAdes assembler.", type = str,  default = "")
   groupAssembly.add_argument("--a5_params", help = "Additional parameters used for A5 assembler.", type = str, default = "")

   # Evaluation
   
   groupEval = parser.add_argument_group("Evaluation") 
   groupEval.add_argument("-r", "--reference", help = "Fasta file with reference sequence (e.g. Sanger sequence).", type = str, action = "store", default = "")
   groupEval.add_argument("--genome_size", help = "Expected size of assembled contigs (genome).", type = str, default = "")
   groupEval.add_argument("--pilon_params", help = "Additional parameters for Pilon.", type = str, default = "")
   groupEval.add_argument("--bowtie2_params", help = "Additional parameters for bowtie2.", type = str, default = "")
   groupEval.add_argument("--quast_params", help = "Additional parameters for Quast.", type = str, default = "")
   groupEval.add_argument("--nucmer_params", help = "Additional parameters for nucmer.", type = str, default = "")

   # Other
   
   groupOther = parser.add_argument_group("Other") 
   groupOther.add_argument("-t", "--threads", help = "Number of parallel processes. (Default: 1)", type = str, default = "1")
   groupOther.add_argument("-v", "--version", help = "Prints current version of the pipeline.", action = "version", version = "Assembly pipeline version " + __version__)
     
   args = parser.parse_args()    

   inputPairedFw = {"pairedfw" : {}}
   inputPairedRev = {"pairedrev" : {}}
   inputUnpaired = {"unpaired" : {}}
   configData = {}
   samples = set()
   setParameters = True

   if not os.path.exists(os.path.abspath(args.output)):
      os.makedirs(os.path.abspath(args.output))

   if not os.path.exists(os.path.abspath(args.log)):
      os.makedirs(os.path.abspath(args.log))


   logFile = os.path.abspath(args.log) + "/" + "assembly_" + datetime.datetime.strftime(datetime.datetime.now(),"%d.%m.%Y_%H-%M-%S") + ".log"

   configData.update({"logfile" : logFile})


   logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO , filename=logFile, filemode = "a+")

   console = logging.StreamHandler()
   console.setLevel(logging.INFO)

   formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
   console.setFormatter(formatter)
   logging.getLogger("").addHandler(console)

   # ----- input ------ #

   configData["threads"] = args.threads

   # check trimmed input folder from QC output
     
   if args.input or args.read1 or args.read2 or args.unpaired:

      configData.update({"type" : []})

      if args.input:
         logging.info("Reading provided input folder")
       
         all_files = [file for file in os.listdir(args.input) if file.endswith(".fastq") or file.endswith(".fastq.gz")]
   
         if all_files:
   
            for f in all_files:
            
               if f.startswith("r1_") or f.startswith("r2_"): # trimmed folder
                  samples.add(re.sub("_L\d{3}_R\d(_\d{3})?.fastq(.gz)?","",f).replace("r1_P_","").replace("r2_P_", "").replace("r1_UP_", "").replace("r2_UP_", ""))
               else: # original data folder
                  samples.add(re.sub("_L\d{3}_R\d(_\d{3})?.fastq(.gz)?","",f))
   
            # map dictionary for sample names
            sampleMapping = {}
            for s in samples:
               sampleMapping[s] = s
            
            if args.sampleNamesMap:
               mapFile = tools.utils.check_file(args.sampleNamesMap)
                        
               with open(mapFile, "r") as f:
                  for line in f:
                     mapping = line.rstrip().split("\t") 
                     if mapping[0] in sampleMapping.keys():
                        sampleMapping[mapping[0]] = mapping[1]  
                           
            
            for s in samples:
   
               inputPairedFw["pairedfw"].update({sampleMapping[s] : []})
               inputPairedRev["pairedrev"].update({sampleMapping[s] : []}) 
               inputUnpaired["unpaired"].update({sampleMapping[s] : []})
   
               fw = []
               rev = []
               up = []
               
               for f in all_files:
                  
                  if re.search(s + "_L\d{3}_R1",f):
                     if f.startswith("r1_P_" + s): # trimmed folder
                        fw.append(tools.utils.check_file(args.input + "/" + f))
                     elif f.startswith("r1_UP_" +  s):
                        up.append(tools.utils.check_file(args.input + "/" + f))
                     else:
                        fw.append(tools.utils.check_file(args.input + "/" + f))
                     
                  if re.search(s + "_L\d{3}_R2",f):
                     if f.startswith("r2_P_" + s): # trimmed folder
                        rev.append(tools.utils.check_file(args.input + "/" + f))
                     elif f.startswith("r2_UP_" +  s):
                        up.append(tools.utils.check_file(args.input + "/" + f))
                     else:
                        rev.append(tools.utils.check_file(args.input + "/" + f))
                        
               if rev:
                  if not "paired" in configData["type"]:
                     configData["type"].append("paired")
                 
                  inputPairedFw["pairedfw"][sampleMapping[s]] = fw
                  inputPairedRev["pairedrev"][sampleMapping[s]] = rev 
                  
                  if up:
                     if not "unpaired" in configData["type"]:
                        configData["type"].append("unpaired")
                     inputUnpaired["unpaired"][sampleMapping[s]] = up
                  
               else:
                  if not "unpaired" in configData["type"]:
                     configData["type"].append("unpaired")            
   
                  inputPairedFw["unpaired"][sampleMapping[s]] = fw 
                  inputUnpaired["unpaired"][sampleMapping[s]].extend(up)             
         
         # check if nr of forward files = number of reverse files!!!
         
         else:
            #raise argparse.ArgumentTypeError("No fastq or fastq.gz files found in the given input folder!")
            logging.error("No fastq or fastq.gz files found in the given input folder!")
            sys.exit()
                 
      else:
         
         fwFiles = []
         revFiles = []
         
         sampleName = args.sampleName
            
         inputPairedFw["pairedfw"].update({sampleName : []})
         inputPairedRev["pairedrev"].update({sampleName : []}) 
         inputUnpaired["unpaired"].update({sampleName : []})  
         
         if args.read1 or args.read2:
            
            if not args.read2:
               logging.error("Missing reverse file for provided single sample!")
               sys.exit()
               
            if not args.read1:
               logging.error("Missing forward file for provided single sample!")
               sys.exit()
            
            fwFiles = args.read1.split(",")
            revFiles = args.read2.split(",")
                    
            
            if len(fwFiles) != len(revFiles):
               logging.error("Number of forward and reverse files differs!")
               sys.exit()  
          
            if not "paired" in configData["type"]:
               configData["type"].append("paired")
         
         inputPairedFw["pairedfw"][sampleName] = [tools.utils.check_file(file) for file in fwFiles]
         inputPairedRev["pairedrev"][sampleName] = [tools.utils.check_file(file) for file in revFiles]

         if args.unpaired:
            
            if not "unpaired" in configData["type"]:
               configData["type"].append("unpaired")  
            inputUnpaired["unpaired"][sampleName] = [tools.utils.check_file(file) for file in args.unpaired.split(",")]

   elif args.config:
      logging.info("A config file was provided. All other parameter settings will be ignored!")
      configFile = tools.utils.check_file(args.config)
      setParameters = False  
      
   else:
      logging.error("No input data was specified!") 
      sys.exit()

   
   if setParameters:  
      allSamples = set(list(inputPairedFw["pairedfw"].keys()) + list(inputPairedRev["pairedrev"].keys()) + list(inputUnpaired["unpaired"].keys()))
      configData.update({"samples" : [s for s in allSamples]})
      configData.update(inputPairedFw)
      configData.update(inputPairedRev)
      configData.update(inputUnpaired)

      # ----- output ------ #
   
      outputPath = os.path.abspath(args.output)
      if not os.path.exists(outputPath):
         logging.info("Output path " + outputPath + " does not exist. Create folder.")
         os.makedirs(args.output)
       
      # ----- parameters ------ #
   
      # assembler
      assembler = [a for a in args.assemblytools.split(",")]
      
      if not "paired" in configData["type"]:
         if "a5" in assembler:
            logging.info("Given dataset is unpaired. A5 assembler is only available for paired data and will be ignored.")
            assembler.remove("a5")
      
      configData.update({"assembler" : assembler})
      
      if "spades" in assembler:
         configData.update({"spadesParams" : {}})
      if "velvet" in assembler:
         configData.update({"velvetOptHashLengths" : {}})
         configData.update({"velvetOptParams" : {}})
         configData.update({"velvethParams" : {}})
         configData.update({"velvetgParams" : {}})
      if "a5" in assembler:
         configData.update({"a5Params" : {}})   
      
      
      for s in allSamples:
         
         if "spades" in assembler:
            spadesParams = [p for p in args.spades_params.split(" ") if p]
            if "--careful" not in spadesParams:
               spadesParams.append("--careful")
            if "--cov-cutoff" not in spadesParams:
               spadesParams.extend(["--cov-cutoff", "auto"])
            configData["spadesParams"][s] = spadesParams
         if "velvet" in assembler:
            configData["velvetOptHashLengths"][s] = [p for p in args.velvetopt_hash_lengths.split(" ") if p]
            configData["velvetOptParams"][s] = [p for p in args.velvetopt_params.split(" ") if p]
            configData["velvethParams"][s] = [p for p in args.velveth_params.split(" ") if p]
            configData["velvetgParams"][s] = [p for p in args.velvetg_params.split(" ") if p]
         if "a5" in assembler:
            configData["a5Params"][s] = [p for p in args.a5_params.split(" ") if p]
         
         
      configData.update({"genomeSize" : args.genome_size})
     
      # subsampling
      if args.no_subsampling or not args.genome_size:
         configData.update({"subsampling" : "False"})   
         logging.info("No subsampling will be performed either due to missing genome size or option --no_subsampling was selected.")  
      else:
         configData.update({"subsampling" : "True"})
         configData.update({"coverage" : args.coverage})
         
      configData["speciesFg"] = args.rambok_fg_name
      # RAMBO-K
      if args.rambok_fg or args.rambok_bg or args.rambok_params:
         
         
         logging.info("Running RAMBO-K was selected.")
         # test all the other parameters
         configData.update({"runRambok" : "True"})
         
         if not args.rambok_fg or not args.rambok_bg:
            #raise ValueError("Some parameters for RAMBO-K were specified, but some are missing!")
            logging.error("Some parameters for RAMBO-K were specified, but some are missing!")
            sys.exit(0)
         
         configData.update({"refFg" : {}})
         configData.update({"refBg" : {}})
         configData.update({"rambokParams" : {}})
         
         for s in allSamples:
            
            configData["refFg"][s] = tools.utils.check_file(args.rambok_fg)
            configData["refBg"][s] = tools.utils.check_file(args.rambok_bg)
            configData["rambokParams"][s] = [p for p in args.rambok_params.split(" ") if p]
            if args.rambok_k:
               configData["rambokParams"][s].extend(["-k", args.rambok_k])
            if args.rambok_cutoff:
               configData["rambokParams"][s].extend(["-C", args.rambok_cutoff])
         
            
      else:
         configData.update({"runRambok" : "False"})
         if args.rambok_test:
            #raise argparse.ArgumentTypeError("RAMBO-K test run was selected but one or some of the required parameters are missing.")
            logging.error("RAMBO-K test run was selected but one or some of the required parameters are missing.")
   
      # other tools
      configData.update({"pilonParams" : {}})
      configData.update({"bowtie2Params" : {}})
      configData.update({"quastParams" : {}})
      configData.update({"nucmerParams" : {}})
      
      
      bowtie2Params = [p for p in args.bowtie2_params.split(" ") if p]
        
      if not "--no-unal" in bowtie2Params:
         bowtie2Params.append("--no-unal")
      
      pilonParams = [p for p in args.pilon_params.split(" ") if p]
      
      if not "--vcf" in pilonParams:
         pilonParams.append("--vcf")
      
      if not "--changes" in pilonParams:
         pilonParams.append("--changes")
      
      for s in allSamples:
          
         configData["pilonParams"][s] = pilonParams
         configData["bowtie2Params"][s] = bowtie2Params
         configData["quastParams"][s] = [p for p in args.quast_params.split(" ") if p]
         configData["nucmerParams"][s] = [p for p in args.nucmer_params.split(" ") if p]
   
      if args.reference:
         ref = tools.utils.check_file(args.reference)
         configData.update({"nucmerRef" : ref})
      else:
         configData.update({"nucmerRef" : ""})
         
         
      if "paired" in configData["type"]:
         configData["read1"] = ["R1"]
         configData["read2"] = ["R2"]
         configData["pairedtype"] = ["paired"]
      else:
         configData["read1"] = []
         configData["read2"] = []
         configData["pairedtype"] = []
         
      if "unpaired" in configData["type"]:
         configData["up"] = ["UP"]
         configData["unpairedtype"] = ["unpaired"]
      else:
         configData["up"] = []
         configData["unpairedtype"] = []

      configPath = outputPath + "/config.yaml"
      with open(configPath, "w") as f:
         yaml.dump(configData, f)
      
   else:
      configPath = configFile

             
   # call rambok in case of testing parameters   
   if args.rambok_test:
      
      targetFiles = [s + "/report/rambok_eval.txt" for s in sampleMapping.values()]
      
      snakemakeArgs = ["snakemake",
                    "--configfile", configPath,
                    "--snakefile", os.path.join(os.path.dirname(__file__), "Snakefile"),
                     "--directory", os.path.abspath(args.output),
                     "--jobs", args.threads]
      snakemakeArgs.extend(targetFiles)

   else:           
      snakemakeArgs = ["snakemake",
                       "--configfile", configPath,
                       "--snakefile", os.path.join(os.path.dirname(__file__), "Snakefile"),
                        "--directory", os.path.abspath(args.output),
                        "--jobs", args.threads,
                        "--use-conda",
                        "--latency-wait", "60"]
  
   
  # root = logging.getLogger()
  # hdlr = root.handlers[0]
  # fmt = logging.Formatter("") # change format of snakemake logging output
  # hdlr.setFormatter(fmt)
  # console.setFormatter(fmt)
   
  # with subprocess.Popen(snakemakeArgs, stdout = subprocess.PIPE, stderr = subprocess.STDOUT, bufsize = 1) as p:
  #    for line in p.stdout:
  #       logging.info(line.decode("utf-8").strip())
   
   
   snakemake = subprocess.Popen(snakemakeArgs)
   snakemake.communicate()
 

if __name__ == '__main__':
   main()