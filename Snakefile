import tools, time

############ input functions for rules "rambok_eval" and "rambok" ############ 

def get_paired_rambok_input_files(wildcards):
   if "paired" in config["type"]:
      return ["{wildcards.sample}/concat_R1_{wildcards.sample}.fastq".format(wildcards=wildcards), 
              "{wildcards.sample}/concat_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
   else:
      return []

def get_unpaired_rambok_input_file(wildcards):
   return ["{wildcards.sample}/concat_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []

############ input functions for rule "subsample" ############ 

def get_paired_subsample_input_files(wildcards):   
   if "paired" in config["type"]:
      if config["runRambok"] == "True":
         return ["{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + config["speciesFg"] + "_R1_{wildcards.sample}.fastq".format(wildcards=wildcards), 
                 "{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + config["speciesFg"] + "_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
      else:
         return ["{wildcards.sample}/concat_R1_{wildcards.sample}.fastq".format(wildcards=wildcards), 
                 "{wildcards.sample}/concat_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
   else:
      return []

def get_unpaired_subsample_input_file(wildcards):
   if config["runRambok"] == "True":
      return ["{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + config["speciesFg"] + "_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []
   else:
      return ["{wildcards.sample}/concat_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []

############ input functions for rule "assembly" ############ 

def get_paired_assembly_input_files(wildcards):
   if "paired" in config["type"]:
      if config["subsampling"] == "True":
         return ["{wildcards.sample}/files/subsampling/subsampled_concat_R1_{wildcards.sample}.fastq".format(wildcards=wildcards),
                 "{wildcards.sample}/files/subsampling/subsampled_concat_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
      elif config["runRambok"] == "True":
         return ["{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + config["speciesFg"] + "_R1_{wildcards.sample}.fastq".format(wildcards=wildcards), 
                 "{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + config["speciesFg"] + "_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
      else:
         return ["{wildcards.sample}/concat_R1_{wildcards.sample}.fastq".format(wildcards=wildcards),
                 "{wildcards.sample}/concat_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
   else:
      return []
   
def get_unpaired_assembly_input_file(wildcards):
   if config["subsampling"] == "True":
      return ["{wildcards.sample}/files/subsampling/subsampled_concat_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []
   elif config["runRambok"] == "True":
      return ["{wildcards.sample}/files/RAMBO-K/".format(wildcards=wildcards) + str(config["speciesFg"]) + "_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []
   else:
      return ["{wildcards.sample}/concat_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []

############ input functions for rule "bowtie2_raw" and "bowtie2_final" ############ 

def get_paired_mapping_input_files(wildcards):
   if "paired" in config["type"]:
      return ["{wildcards.sample}/concat_R1_{wildcards.sample}.fastq".format(wildcards=wildcards),
              "{wildcards.sample}/concat_R2_{wildcards.sample}.fastq".format(wildcards=wildcards)]
   else:
      return []

def get_unpaired_mapping_input_file(wildcards):
   return ["{wildcards.sample}/concat_UP_{wildcards.sample}.fastq".format(wildcards=wildcards)] if "unpaired" in config["type"] else []


############ input functions for rule "pilon" ############ 

def get_paired_pilon_input_files(wildcards):
   return ["{wildcards.sample}/results/{wildcards.assembler}/bowtie2_raw_results/mapping_results_raw_paired_{wildcards.sample}_sorted.bam".format(wildcards=wildcards)] if "paired" in config["type"] else []

def get_unpaired_pilon_input_file(wildcards): 
   return ["{wildcards.sample}/results/{wildcards.assembler}/bowtie2_raw_results/mapping_results_raw_unpaired_{wildcards.sample}_sorted.bam".format(wildcards=wildcards)] if "unpaired" in config["type"] else []

############ input functions for rule "nucmer" ############ 

def get_nucmer_report_input_file(wildcards):
   return ["{wildcards.sample}/report/".format(wildcards=wildcards) + ass + "_nucmer_plot_report.tsv" for ass in config["assembler"]] if config["nucmerRef"] else []

############ input functions for rule "report" ############ 

def get_rambok_report_input_file(wildcards):
   return ["{wildcards.sample}/report/RAMBO-K_filter_report.tsv".format(wildcards=wildcards)] if config["runRambok"] == "True" else []

def get_subsample_report_input_file(wildcards):
   return ["{wildcards.sample}/report/subsampling_stats.tsv".format(wildcards=wildcards)] if config["subsampling"] == "True" else []

########################################################## 

rule all:
   input:
      #expand("{sample}/tmp/subsampled_concat_{read}_{sample}.fastq", sample = config["samples"], read = config["readup"]),
      expand("{sample}/report/{assembler}_assembly_report.tsv", sample = config["samples"], assembler = config["assembler"]),
      #expand("{sample}/{assembler}_results/bowtie2_final_results/mapping_results_{seqtype}_{sample}.bam", sample = config["samples"], assembler = config["assembler"], seqtype = config["type"]),
      #expand("{sample}/{assembler}_results/corrected_contigs/contigs_{sample}.fa", sample = config["samples"], assembler = config["assembler"]),   
      #expand("{sample}/{assembler}_results/bowtie2_final_results/mapping_results_{sample}_complete_sorted.png", sample = config["samples"], assembler = config["assembler"]),
      #expand("{sample}/results/pilon/{assembler}/corrected_contigs/nucmer_plot_{sample}.png", sample = config["samples"], assembler = config["assembler"]),
   #   expand("{sample}/{assembler}_results/bowtie2_final_results/mapping_results_{sample}_complete_sorted.png", sample = config["samples"], assembler = config["assembler"]),
      expand("Assembly_report_{sample}.pdf", sample = config["samples"])     
      #expand("{sample}/rambok_results/rambok_eval.txt", sample = config["samples"])

rule collect_pipeline_info:
   """starting pipeline and collecting server/user information"""  
   output: 
      stats = "pipeline_report.tsv"
   run:
      tools.utilitytools.collect_pipeline_info("Assembly", "0.0.1", output.stats)

rule concat_files:
   """concat files and get information about number of reads, bases etc."""
   input:
      "pipeline_report.tsv",
      pairedfw = lambda wildcards: config["pairedfw"][wildcards.sample],
      pairedrev = lambda wildcards: config["pairedrev"][wildcards.sample],
      unpaired = lambda wildcards: config["unpaired"][wildcards.sample]
   output:
      pairedfw = temp(expand("{{sample}}/concat_{r1}_{{sample}}.fastq", r1 = config["read1"])),
      pairedrev = temp(expand("{{sample}}/concat_{r2}_{{sample}}.fastq", r2 = config["read2"])),
      unpaired = temp(expand("{{sample}}/concat_{up}_{{sample}}.fastq", up = config["up"])),
      stats = "{sample}/report/sample_stats.tsv"
   params:
      tmpOutput = "{sample}/"
   run:
      outpairedfw = []
      if output.pairedfw:
         outpairedfw = output.pairedfw[0]
      outpairedrev = []
      if output.pairedrev:
         outpairedrev = output.pairedrev[0]
      outunpaired = []
      if output.unpaired:
         outunpaired = output.unpaired[0]
     
      tools.utilitytools.concat_files(wildcards.sample, input.pairedfw, input.pairedrev, input.unpaired, outpairedfw, outpairedrev, outunpaired, params.tmpOutput, output.stats)

rule rambok_eval:
   """evaluate best parameter settings for RAMBO-K - generates plots for a number of kmer lengths"""
   input:
      paired = get_paired_rambok_input_files,
      unpaired = get_unpaired_rambok_input_file
   output:
      stats = "{sample}/report/rambok_eval.txt"
   params:
      pairedOutputPath = "{sample}/results/RAMBO-K/paired/",
      unpairedOutputPath = "{sample}/results/RAMBO-K/unpaired/"
   threads: 
      int(config["threads"])
   run:     
      tools.utilitytools.rambok(wildcards.sample, input.paired, input.unpaired, config["refFg"][wildcards.sample], config["refBg"][wildcards.sample], 
                                config["speciesFg"], params.pairedOutputPath, params.unpairedOutputPath, config["rambokParams"][wildcards.sample], output.stats, "RAMBO-K", log.log, threads)

rule rambok:
   """run RAMBO-K for certain parameter settings"""
   input:
      paired = get_paired_rambok_input_files,
      unpaired = get_unpaired_rambok_input_file
   output:
      pairedfw = expand("{{sample}}/files/RAMBO-K/" + config["speciesFg"] + "_{r1}_{{sample}}.fastq", r1 = config["read1"]),
      pairedrev = expand("{{sample}}/files/RAMBO-K/" + config["speciesFg"] + "_{r2}_{{sample}}.fastq", r2 = config["read2"]),
      unpaired = expand("{{sample}}/files/RAMBO-K/" + config["speciesFg"] + "_{up}_{{sample}}.fastq", up = config["up"]),
      stats = "{sample}/report/RAMBO-K_filter_report.tsv"
   log: 
      log = config["logfile"]
   params:
      pairedOutputPath = "{sample}/results/RAMBO-K/paired/",
      unpairedOutputPath = "{sample}/results/RAMBO-K/unpaired/"
   threads: 
      int(config["threads"])
   run:
      outpairedfw = []
      if output.pairedfw:
         outpairedfw = output.pairedfw[0]
      outpairedrev = []
      if output.pairedrev:
         outpairedrev = output.pairedrev[0]
      outunpaired = []
      if output.unpaired:
         outunpaired = output.unpaired[0]
      tools.utilitytools.rambok(wildcards.sample, input.paired, input.unpaired, 
                                config["refFg"][wildcards.sample], config["refBg"][wildcards.sample], config["speciesFg"], 
                                params.pairedOutputPath, params.unpairedOutputPath, config["rambokParams"][wildcards.sample], output.stats, "RAMBO-K", log.log, threads,
                                pairedFwOutputFile = outpairedfw, pairedRevOutputFile = outpairedrev, unpairedOutputFile = outunpaired)

rule subsample:
   """Subsample reads if coverage is too high"""
   input:
      paired = get_paired_subsample_input_files,
      unpaired = get_unpaired_subsample_input_file
   output:
      pairedfw = expand("{{sample}}/files/subsampling/subsampled_concat_{r1}_{{sample}}.fastq", r1 = config["read1"]), 
      pairedrev = expand("{{sample}}/files/subsampling/subsampled_concat_{r2}_{{sample}}.fastq", r2 = config["read2"]),
      unpaired = expand("{{sample}}/files/subsampling/subsampled_concat_{up}_{{sample}}.fastq", up = config["up"]),
      stats = "{sample}/report/subsampling_stats.tsv"
   run:
      outpairedfw = []
      if output.pairedfw:
         outpairedfw = output.pairedfw[0]
      outpairedrev = []
      if output.pairedrev:
         outpairedrev = output.pairedrev[0]
      outunpaired = []
      if output.unpaired:
         outunpaired = output.unpaired[0]
      tools.utilitytools.subsample(wildcards.sample, config["genomeSize"], config["coverage"], 
                                   input.paired, input.unpaired,
                                   outpairedfw, outpairedrev, outunpaired, output.stats, "Subsampling")

rule spades:
   """De novo assembly of reads using SPAdes"""
   input:
      paired = get_paired_assembly_input_files,
      unpaired = get_unpaired_assembly_input_file
   output:
      contigs = "{sample}/results/spades/raw_contigs/raw_contigs_{sample}.fa",
      stats = "{sample}/report/spades_assembly_report.tsv"
   params:
      outputPath = "{sample}/results/spades/",
   threads: 
      int(config["threads"])
   run:
      tools.assemblytools.spades(wildcards.sample, input.paired, input.unpaired, config["spadesParams"][wildcards.sample],
                                    config["genomeSize"], params.outputPath, output.contigs, output.stats, "SPAdes", threads)

rule velvet:
   """De novo assembly of reads using velvet"""
   input:
      paired = get_paired_assembly_input_files,
      unpaired = get_unpaired_assembly_input_file
   output:
      contigs = "{sample}/results/velvet/raw_contigs/raw_contigs_{sample}.fa",
      stats = "{sample}/report/velvet_assembly_report.tsv"
   params:
      outputPath = "{sample}/results/velvet/",     
   threads: 
      int(config["threads"])
   run:
      tools.assemblytools.velvet(wildcards.sample, input.paired, input.unpaired, config["velvetOptHashLengths"][wildcards.sample], 
                                 config["velvetOptParams"][wildcards.sample], config["velvethParams"][wildcards.sample], config["velvetgParams"][wildcards.sample],
                                 config["genomeSize"], params.outputPath, output.contigs, output.stats, "Velvet", threads)


rule a5:
   """De novo assembly of reads using A5"""
   input:
      paired = get_paired_assembly_input_files
   output:
      contigs = "{sample}/results/a5/raw_contigs/raw_contigs_{sample}.fa",
      stats = "{sample}/report/a5_assembly_report.tsv"
   params:
      outputPath = "{sample}/results/a5/",
   threads: 
      int(config["threads"])
   run:
      tools.assemblytools.afive(wildcards.sample, input.paired, config["a5Params"][wildcards.sample], config["genomeSize"], params.outputPath, output.contigs, output.stats, "A5", threads)


rule bowtie2_raw_index:
   """Generate bowtie2 index for assembled contigs"""
   input:
      contigs = "{sample}/results/{assembler}/raw_contigs/raw_contigs_{sample}.fa"
   output:
      expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/bowtie2_index/raw_contigs_{{sample}}.{index}.bt2", index = range(1,5)),
      expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/bowtie2_index/raw_contigs_{{sample}}.rev.{index}.bt2", index = range(1,3))
   params:
      indexBase = "{sample}/results/{assembler}/bowtie2_raw_results/bowtie2_index/raw_contigs_{sample}"
   threads: 
      int(config["threads"])
   run:
      tools.mappingtools.bowtie2_index(wildcards.sample, input.contigs, params.indexBase, threads)

rule bowtie2_raw:
   """Map original reads to assembled contigs"""
   input:
      paired = get_paired_mapping_input_files,
      unpaired = get_unpaired_mapping_input_file,
      index1 = lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/bowtie2_index/raw_contigs_{sample}.{index}.bt2", index = range(1,5), sample = wildcards.sample, assembler = wildcards.assembler),
      index2 = lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/bowtie2_index/raw_contigs_{sample}.rev.{index}.bt2", index = range(1,3), sample = wildcards.sample, assembler = wildcards.assembler)
   output:
      pairedsam = expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{{sample}}.sam", seqtype = config["pairedtype"]),
      unpairedsam = expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{{sample}}.sam", seqtype = config["unpairedtype"]),
      stats = "{sample}/report/{assembler}_bowtie2_raw_mapping_report.tsv"
   params:
      indexBase = "{sample}/results/{assembler}/bowtie2_raw_results/bowtie2_index/raw_contigs_{sample}",
      outputPath = "{sample}/results/{assembler}/bowtie2_raw_results/"
   threads: 
      int(config["threads"])
   run:
      outpairedsam = []
      if output.pairedsam:
         outpairedsam = output.pairedsam[0]
      outunpairedsam = []
      if output.unpairedsam:
         outunpairedsam = output.unpairedsam[0]
      tools.mappingtools.bowtie2(wildcards.sample, input.paired, input.unpaired, outpairedsam, outunpairedsam, params.indexBase, 
                                 config["bowtie2Params"][wildcards.sample], params.outputPath, output.stats, wildcards.assembler + "_bowtie2_raw", threads)
      
rule sam_to_bam_raw:
   """Convert sam files to bam files"""
   input:
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{sample}.sam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      bams = temp(expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{{sample}}.bam", seqtype = config["type"])),
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.sam_to_bam(input, threads)

rule sort_bam_raw:
   """Sort bam files"""
   input: 
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{sample}.bam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{{sample}}_sorted.bam", seqtype = config["type"])
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.sort_bam(input, threads)
      
rule index_bam:
   """Index bam files"""
   input:
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{sample}_sorted.bam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      expand("{{sample}}/results/{{assembler}}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{{sample}}_sorted.bai", seqtype = config["type"])
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.index_bam(input, threads)
     
   # mark duplicates!!!???  
      
rule pilon:
   """Detection of misassemblies"""
   input:
      pairedbam =  get_paired_pilon_input_files,
      unpairedbam =  get_unpaired_pilon_input_file,
      contigs = "{sample}/results/{assembler}/raw_contigs/raw_contigs_{sample}.fa",
      indexedbams = lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_raw_results/mapping_results_raw_{seqtype}_{sample}_sorted.bai", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      contigs = "{sample}/results/pilon/{assembler}/corrected_contigs/final_contigs_{sample}.fa",
      stats = "{sample}/report/{assembler}_pilon_correction_report.tsv"
   params: 
      outputDir = "{sample}/results/pilon/{assembler}/",
      outPrefix = "final_contigs_{sample}"
   run: 
      tools.evaluationtools.pilon(wildcards.sample, input.contigs, input.pairedbam, 
                                  input.unpairedbam, params.outPrefix, params.outputDir, 
                                  config["pilonParams"][wildcards.sample], output.contigs, output.stats, wildcards.assembler + "_pilon")

      # --fix all,amb,breaks
      # --iupac
      # --threads is experimental
      
rule bowtie2_final_index:
   """Generate bowtie2 index for corrected contigs"""
   input:
      contigs = "{sample}/results/pilon/{assembler}/corrected_contigs/final_contigs_{sample}.fa"
   output:
      expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/bowtie2_index/final_contigs_{{sample}}.{index}.bt2", index = range(1,5)),
      expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/bowtie2_index/final_contigs_{{sample}}.rev.{index}.bt2", index = range(1,3))
   params:
      indexBase = "{sample}/results/{assembler}/bowtie2_final_results/bowtie2_index/final_contigs_{sample}"
   threads: 
      int(config["threads"])
   run:
      tools.mappingtools.bowtie2_index(wildcards.sample, input.contigs, params.indexBase, threads)   

rule bowtie2_final:
   """Map original reads to corrected contigs"""
   input:
      paired = get_paired_mapping_input_files,
      unpaired = get_unpaired_mapping_input_file,
      index1 = lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_final_results/bowtie2_index/final_contigs_{sample}.{index}.bt2", index = range(1,5), sample = wildcards.sample, assembler = wildcards.assembler),
      index2 = lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_final_results/bowtie2_index/final_contigs_{sample}.rev.{index}.bt2", index = range(1,3), sample = wildcards.sample, assembler = wildcards.assembler)
   output:
      pairedsam = expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/mapping_results_final_{seqtype}_{{sample}}.sam", seqtype = config["pairedtype"]),
      unpairedsam = expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/mapping_results_final_{seqtype}_{{sample}}.sam", seqtype = config["unpairedtype"]),
      stats = "{sample}/report/{assembler}_bowtie2_final_mapping_report.tsv"
   params:
      indexBase = "{sample}/results/{assembler}/bowtie2_final_results/bowtie2_index/final_contigs_{sample}",
      outputPath = "{sample}/results/{assembler}/bowtie2_final_results/"
   threads: 
      int(config["threads"])
   run:
      outpairedsam = []
      if output.pairedsam:
         outpairedsam = output.pairedsam[0]
      outunpairedsam = []
      if output.unpairedsam:
         outunpairedsam = output.unpairedsam[0]
      tools.mappingtools.bowtie2(wildcards.sample, input.paired, input.unpaired, outpairedsam, outunpairedsam, 
                                 params.indexBase, config["bowtie2Params"][wildcards.sample], params.outputPath, output.stats,  wildcards.assembler + "_bowtie2_final", threads)
   
rule sam_to_bam_final:
   """Convert sam files to bam files"""
   input:
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_final_results/mapping_results_final_{seqtype}_{sample}.sam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      temp(expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/mapping_results_final_{seqtype}_{{sample}}.bam", seqtype = config["type"])),
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.sam_to_bam(input, threads)

rule sort_bam_final:
   """Sort bam files"""
   input: 
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_final_results/mapping_results_final_{seqtype}_{sample}.bam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      temp(expand("{{sample}}/results/{{assembler}}/bowtie2_final_results/mapping_results_final_{seqtype}_{{sample}}_sorted.bam", seqtype = config["type"]))
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.sort_bam(input, threads)

rule merge_bams:
   """Merge bam files"""
   input:
      lambda wildcards: expand("{sample}/results/{assembler}/bowtie2_final_results/mapping_results_final_{seqtype}_{sample}_sorted.bam", sample = wildcards.sample, assembler = wildcards.assembler, seqtype = config["type"])
   output:
      file = "{sample}/results/{assembler}/bowtie2_final_results/mapping_results_final_{sample}_complete_sorted.bam"
   threads: 
      int(config["threads"])
   run:
      tools.utilitytools.merge_bams(input, output.file, threads)

rule quast:
   """Evaluate and compare assemblies"""
   input:
      raw =  lambda wildcards: expand("{sample}/results/{assembler}/raw_contigs/raw_contigs_{sample}.fa", sample = wildcards.sample, assembler = config["assembler"]),
      final =  lambda wildcards: expand("{sample}/results/pilon/{assembler}/corrected_contigs/final_contigs_{sample}.fa", sample = wildcards.sample, assembler = config["assembler"])
   output:
      stats = "{sample}/report/quast_evaluation_report.tsv"
   #conda:
   #   "quast_env.yml"
   params:
      outputPath = "{sample}/results/quast/"
   threads: 
      int(config["threads"])
   #script:
   #   "tools/quast.py"
   run:
      # it has to be checked if labels are correct!!!!!
      tools.evaluationtools.quast(wildcards.sample, input.raw + input.final, ",".join([l + "_raw" for l in config["assembler"]]) + "," + ",".join([l + "_final" for l in config["assembler"]]), config["quastParams"][wildcards.sample], params.outputPath, output.stats, "QUAST", threads)
      # provide optional reference/Sanger genome

rule nucmer:
   """Plot multiple alignments to reference file, if provided"""
   input:
      queryfile = "{sample}/results/pilon/{assembler}/corrected_contigs/final_contigs_{sample}.fa"
   output:
      stats = "{sample}/report/{assembler}_nucmer_plot_report.tsv"
   params:
      nameBase = "{sample}/results/pilon/{assembler}/corrected_contigs/nucmer_plot_{sample}"
   run:
      tools.utilitytools.nucmer(config["nucmerRef"], input.queryfile, params.nameBase, config["nucmerParams"][wildcards.sample], output.stats, wildcards.assembler + "_NUCmer")
      # make this step optional depending on reference
      
rule cov_stats:
   """Calculate and plot coverage for contigs"""
   input:
      file = "{sample}/results/{assembler}/bowtie2_final_results/mapping_results_final_{sample}_complete_sorted.bam"
   output:
      stats = "{sample}/report/{assembler}_coverage_stats.tsv"
   run:
      tools.utilitytools.cov_stats(input.file, output.stats, wildcards.assembler + "_coverage_stats")

   
rule report:
   """Generate analysis report"""
   input:
      "pipeline_report.tsv",
      "{sample}/report/sample_stats.tsv",
      get_rambok_report_input_file,
      get_subsample_report_input_file,
      lambda wildcards: expand("{sample}/report/{assembler}_assembly_report.tsv", sample = wildcards.sample, assembler = config["assembler"]),
      lambda wildcards: expand("{sample}/report/{assembler}_bowtie2_raw_mapping_report.tsv", sample = wildcards.sample, assembler = config["assembler"]),
      lambda wildcards: expand("{sample}/report/{assembler}_pilon_correction_report.tsv", sample = wildcards.sample, assembler = config["assembler"]),
      lambda wildcards: expand("{sample}/report/{assembler}_bowtie2_final_mapping_report.tsv", sample = wildcards.sample, assembler = config["assembler"]),
      "{sample}/report/quast_evaluation_report.tsv",
      get_nucmer_report_input_file,
      lambda wildcards: expand("{sample}/report/{assembler}_coverage_stats.tsv", sample = wildcards.sample, assembler = config["assembler"])
   output:
      reportFile = "Assembly_report_{sample}.pdf"
   run:
      tools.utils.generate_latex_report(input[0], input[1], input[2:], "Assembly_template", output.reportFile, os.getcwd())
      
      
# get all tool versions from config.ini
# generate consensus sequence of contigs      
   
