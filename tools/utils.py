import os, configparser, subprocess, warnings, datetime, jinja2


def generate_latex_report(pipelineStats, sampleStats, toolStats, templateName, reportName, reportPath):
  
   pipelineInfo = {}
   if pipelineStats:
      with open(pipelineStats, "r") as f:
            
         for line in f:
               
            vals = line.rstrip().split(sep="\t")
            pipelineInfo[vals[0]] = str(vals[1]).replace("#","\#").replace("_", "\_").replace("\t", " ").replace("%", "\%").replace("\n", " ")
              
         pipelineInfo["finished"] = datetime.datetime.strftime(datetime.datetime.now(),"%d.%m.%Y, %H:%M:%S")
  

   sampleInfo = {}
   if sampleStats:
      with open(sampleStats, "r") as f:
            
         for line in f:
               
            vals = line.rstrip().split(sep="\t")
            sampleInfo[vals[0]] = str(vals[1]).replace("#","\#").replace("_", "\_").replace("\t", " ").replace("%", "\%").replace("\n", " ")
      
   

   toolInfo = {}
   for toolFile in toolStats:
      toolName = ""
      tool = {}
      with open(toolFile, "r") as f:
          
         for line in f:
            
            vals = line.rstrip().split(sep="\t")
          
            tool[vals[0]] = str(vals[1]).replace("#","\#").replace("_", "\_").replace("\t", " ").replace("%", "\%").replace("\n", " ")
            if vals[0] == "toolName":
               toolName = vals[1]
      
      #print("---- Information about " +  toolName + " ---")
      #print(tool)
      #print()
      toolInfo[toolName] = tool



   templatesPath = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "templates/")  
   reportEnvironment = jinja2.Environment(variable_start_string='$$', variable_end_string='$$', loader = jinja2.FileSystemLoader(templatesPath))    
   reportContent = reportEnvironment.get_template(templateName + ".tex").render(pipelineInfo = pipelineInfo, sampleInfo = sampleInfo, toolInfo = toolInfo)
       
   fileBase = reportName.replace(".pdf","")     

   reportTex = open(reportPath + "/" + fileBase + ".tex", 'w')    
   reportTex.write(reportContent)    
   reportTex.close()       

   convertToPdf = subprocess.Popen(["pdflatex", "-output-directory", reportPath, fileBase + ".tex"], stdout = subprocess.PIPE)
   convertToPdf.communicate()


   for ext in (".tex",".aux", ".log", ".toc", ".lof", ".lot", ".synctex.gz"):
      if os.path.exists(reportPath + "/" + fileBase + ext):
         os.remove(reportPath + "/" + fileBase + ext)



def parse_config(name, section = "TOOLPATH"):
   
   config = configparser.ConfigParser()
   config.read(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "config.ini"))
   
   return config[section][name]


def export_info(reportInfo, fileName):
    
   with open(fileName, "w") as f:
      f.writelines("{}\t{}\n".format(k, v) for k, v in reportInfo.items())

def remove_redundant_params(param, toolParams, warn = True):
   if param in toolParams:
      if warn:
         warnings.warn("Parameter setting for " + param + " already given. New value will be ignored.")
      del toolParams[toolParams.index(param)+1]  
      del toolParams[toolParams.index(param)]

def cat_files(inputFiles, resultFile):
   
   with open(resultFile,"w") as catFile:
      concat = subprocess.Popen(["cat"] + inputFiles, stdout = catFile)  
      concat.communicate()
      
def gunzip_file(inputFile, output):
   
   newFile = output + inputFile.split("/")[-1].replace(".gz","")

   with open(newFile, "w") as gunzipFile:
      gunzip = subprocess.Popen(["gunzip", "-c", inputFile], stdout = gunzipFile)
      gunzip.communicate()
   
   return newFile

def check_file(file):
    
   if os.path.exists(os.path.abspath(file)):    
      return os.path.abspath(file)
   else:
      raise FileNotFoundError("File " + file + " could not be found!")
   
def calculate_read_stats(files):
  
   nrOfReads = 0
   nrOfBases = 0

   for file in files:

      with open(file, "r") as f:
         count = 2
         for line in f:       
            if count == 3:
                            
               seqLen = len(line.rstrip())
               nrOfBases += seqLen
              
               nrOfReads += 1
               count = -1
            count += 1
         
   
   return nrOfReads, nrOfBases

