import tools, subprocess, shutil, warnings

def pilon(sampleName, contigs, pairedbam, unpairedbam, prefix, outputPath, pilonParams, outputContigs, reportFile, reportName):
   
   print("Run Pilon on mapped contigs to detect misassemblies")
   
   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # --genome
   # --output
   # --outdir
   # --frags
   # --unpaired
   # --jumps
   # --bam
   
   if "--genome" in pilonParams:
      tools.utils.remove_redundant_params("--genome", pilonParams)
   
   if "--output" in pilonParams:
      tools.utils.remove_redundant_params("--output", pilonParams)  
      
   if "--outdir" in pilonParams:
      tools.utils.remove_redundant_params("--outdir", pilonParams)
   
   if "--frags" in pilonParams:
      tools.utils.remove_redundant_params("--frags", pilonParams) 
   
   if "--unpaired" in pilonParams:
      tools.utils.remove_redundant_params("--unpaired", pilonParams)
      
   if "--jumps" in pilonParams:
      warnings.warn("Paired/unpaired input files already specified. Parameter setting for --jumps will be ignored.")
      tools.utils.remove_redundant_params("--jumps", pilonParams, False)

     
   if "--bam" in pilonParams:
      warnings.warn("Paired/unpaired input files already specified. Parameter setting for --bam will be ignored.")
      tools.utils.remove_redundant_params("--bam", pilonParams, False)
 
 
   #---------------------------------------------------------------------------------------- 
   
   #javaRunPath = tools.utils.parse_config(name = "java1.8")
   pilonRunPath = tools.utils.parse_config(name = "pilon")
   
   #pilonVers = str.splitlines(subprocess.Popen([javaRunPath, "-jar", pilonRunPath], stdout = subprocess.PIPE, universal_newlines = True).communicate()[0])[0].replace("Pilon version ", "").replace(" Mon Dec 7 09:45:45 2015 -0500", "")
   pilonVers = str.splitlines(subprocess.Popen([pilonRunPath], stdout = subprocess.PIPE, universal_newlines = True).communicate()[0])[0].replace("Pilon version ", "").replace(" Mon Dec 7 09:45:45 2015 -0500", "")

   reportInfo = {"sampleName" : sampleName,
                 "toolName" : reportName,
                 "pilon_version" : pilonVers}
   
   # pilonArgs = [ javaRunPath, "-jar",
   pilonArgs = [ pilonRunPath,
                  "--genome", contigs,
                  "--output", prefix,
                  "--outdir", outputPath]     
   if pairedbam:
      pilonArgs.extend(["--frags"] + pairedbam)
   if unpairedbam:
      pilonArgs.extend(["--unpaired"] + unpairedbam)
   
   
   pilonArgs.extend(pilonParams)
 
   pilon = subprocess.Popen(pilonArgs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)    
   pilon.communicate()
   
   reportInfo["call"] = " ".join(pilonArgs)
   
   shutil.copy(str(outputPath + "/" + prefix + ".fasta"), str(outputContigs))
   
   tools.utils.export_info(reportInfo, reportFile)

def quast(sampleName, contigs, contigLabels, quastParams, outputPath, reportFile, reportName, threads):
   
   print("Running Quast on assembled contigs: ")
   
   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # -o/--output-dir
   # -l/--labels
   # -t/--threads

   if "-o" in quastParams:
      tools.utils.remove_redundant_params("-o", quastParams)
   
   if "--output-dir" in quastParams:
      tools.utils.remove_redundant_params("--output-dir", quastParams)
   
   if "-l" in quastParams:
      tools.utils.remove_redundant_params("-l", quastParams)
   
   if "--labels" in quastParams:
      tools.utils.remove_redundant_params("--labels", quastParams)
   
   if "-t" in quastParams:
      tools.utils.remove_redundant_params("-t", quastParams)
      
   if "--threads" in quastParams:
      tools.utils.remove_redundant_params("--threads", quastParams)   
      
   #----------------------------------------------------------------------------------------

  
   pythonPath = tools.utils.parse_config(name = "python2")
   quastRunPath = tools.utils.parse_config(name = "quast")
   quastVers = str.splitlines(subprocess.Popen(quastRunPath, stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])[1].replace("Version: ", "")

   reportInfo = {"sampleName" : sampleName,
                 "toolName" : reportName,
                 "quast_version" : quastVers}

   quastArgs = [pythonPath, quastRunPath, 
                "-o", outputPath,
                "-l", contigLabels,
                "-t", str(threads)]

   quastArgs.extend(quastParams)
   quastArgs.extend(contigs)

   quast = subprocess.Popen(quastArgs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   quast.communicate()
   #deactivate = subprocess.Popen(["source", "deactivate", "quast"])

   
   reportInfo["call"] = " ".join(quastArgs)
   
   tableContent = ""
   header = ""
   linePos = 0
        
   # check if report.tsv exists!!!!
        
   with open(outputPath + "/report.tsv", "r") as f:
            
      for line in f:
                       
         resInfo = line.rstrip().split("\t")
         
    
         if linePos == 0:
            header = " & ".join(resInfo)  + "\\\\" 
            linePos += 1
         else:
            tableContent = tableContent + " & ".join(resInfo) + "\\\\" 
         
        
   reportInfo.update({"tableContent" : tableContent,
                      "gcPlot" : outputPath + "/basic_stats/GC_content_plot.pdf",
                      "nxPlot" : outputPath + "/basic_stats/Nx_plot.pdf",
                      "cumPlot" : outputPath + "/basic_stats/cumulative_plot.pdf",
                      "nrOfComparisons" : len(resInfo),
                      "header" : header})

   tools.utils.export_info(reportInfo, reportFile)
 