import datetime, os, subprocess, getpass, sys, tools, warnings, shutil, numpy, random


def collect_pipeline_info(name, version, reportFile):
   
   started = datetime.datetime.strftime(datetime.datetime.now(),"%d.%m.%Y, %H:%M:%S") 
   sysInfo = os.uname()
   snakemakeVersion = (subprocess.Popen([tools.utils.parse_config(name = "snakemake"),"-v"], stdout = subprocess.PIPE, universal_newlines = True).communicate())[0].strip()
   reportInfo = {"pipelineName" : name,
                  "pipelineVersion" : version,
                  "executor" : getpass.getuser(),
                  "started" : started,
                  "server" : sysInfo.nodename,
                  "system" : sysInfo.sysname,
                  "release" : sysInfo.release,
                  "osVersion" : sysInfo.version,
                  "machine" : sysInfo.machine,
                  "pythonVersion" : sys.version.replace("\n"," "),
                  "snakemakeVersion" : snakemakeVersion} # what if sample = None? -not possible in snakemake, but in normal pipeline?
   tools.utils.export_info(reportInfo, reportFile)


def concat_files(sampleName, pairedFwInputFiles, pairedRevInputFiles, unpairedInputFiles, pairedFwOutputFile, pairedRevOutputFile, unpairedOutputFile, tmpOutput, reportFile):
    
   # check which type of input data by file extension (e.g. fastq, fastq.gz, .bam, .sam etc.     
   tmpOut = tmpOutput + "/tmp/"
   os.makedirs(tmpOut, exist_ok=True)
     
   newPairedFwInputFiles = pairedFwInputFiles[:]
   newPairedRevInputFiles = pairedRevInputFiles[:]
   newUnpairedInputFiles = unpairedInputFiles[:]
   
   for file in newPairedFwInputFiles:
      if file.endswith(".fastq.gz"):
         newPairedFwInputFiles[newPairedFwInputFiles.index(file)] = tools.utils.gunzip_file(file, tmpOut)
   
   for file in pairedRevInputFiles:
      if file.endswith(".fastq.gz"):
         newPairedRevInputFiles[newPairedRevInputFiles.index(file)] = tools.utils.gunzip_file(file, tmpOut)
   
   for file in unpairedInputFiles:
      if file.endswith(".fastq.gz"):
         newUnpairedInputFiles[newUnpairedInputFiles.index(file)] = tools.utils.gunzip_file(file, tmpOut)

   print("######### concatenate " +  sampleName + " ###########") 

   # meanReadLength = 0
   # maxReadLength = 0
  
   nrOfPairedReads, noOfPairedBases = tools.utils.calculate_read_stats(newPairedFwInputFiles + newPairedRevInputFiles)
   nrOfUnpairedReads, noOfUnpairedBases = tools.utils.calculate_read_stats(newUnpairedInputFiles)
   totalReads = nrOfPairedReads + nrOfUnpairedReads
   totalBases = noOfPairedBases + noOfUnpairedBases
       
   print("number of paired reads: " + "{:,d}".format(nrOfPairedReads))
   print("number of unpaired reads: " + "{:,d}".format(nrOfUnpairedReads))
   print("number of total reads: " + "{:,d}".format(totalReads))
   
   reportInfo = {"sampleName": sampleName,
                "numberOfPairedReads" : "{:,d}".format(nrOfPairedReads),
                "numberOfPairedBases" : "{:,d}".format(noOfPairedBases),
                "numberOfUnpairedReads": "{:,d}".format(nrOfUnpairedReads),
                "numberOfUnpairedBases" : "{:,d}".format(noOfUnpairedBases)}  
   
   if pairedFwInputFiles:  
          
      tools.utils.cat_files(newPairedFwInputFiles, pairedFwOutputFile)
      reportInfo["pairedFwInputFiles"] = ",".join([file.split("/")[-1] for file in pairedFwInputFiles])
     
      tools.utils.cat_files(newPairedRevInputFiles, pairedRevOutputFile)
      reportInfo["pairedRevInputFiles"] = ",".join([file.split("/")[-1] for file in pairedRevInputFiles])
    
   if unpairedInputFiles:
      tools.utils.cat_files(newUnpairedInputFiles, unpairedOutputFile)
      reportInfo["unpairedInputFiles"] = ",".join([file.split("/")[-1] for file in unpairedInputFiles])
  
   tools.utils.export_info(reportInfo, reportFile)

   shutil.rmtree(tmpOut)

def rambok(sampleName, pairedInputFiles, unpairedInputFile, refFg, refBg, speciesFgName, pairedOutputPath, unpairedOutputPath, rambokParams, reportFile, reportName, logFile, threads, **kwargs):

   print("\n............. Starting RAMBO-K.............")

   evaluate = len(kwargs.items()) == 0

   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # -r/--reffile1
   # -R/--reffile2
   # -o/--outpath
   # -1/--unassigned1
   # -2/--unassigned2  
   # -n/--name1
   #

   if "-r" in rambokParams:
      tools.utils.remove_redundant_params("-r", rambokParams)

   if "--reffile1" in rambokParams:
      tools.utils.remove_redundant_params("--reffile1", rambokParams)
         
   if "-R" in rambokParams:
      tools.utils.remove_redundant_params("-R", rambokParams)
      
   if "--reffile2" in rambokParams:
      tools.utils.remove_redundant_params("--reffile2", rambokParams)

   if "-o" in rambokParams:
      tools.utils.remove_redundant_params("-o", rambokParams)
   
   if "--outpath" in rambokParams:
      tools.utils.remove_redundant_params("--outpath", rambokParams)
      
   if "-1" in rambokParams:
      tools.utils.remove_redundant_params("-1", rambokParams)
      
   if "--unassigned1" in rambokParams:
      tools.utils.remove_redundant_params("--unassigned1", rambokParams)
   
   if "-2" in rambokParams:
      tools.utils.remove_redundant_params("-2", rambokParams)
      
   if "--unassigned2" in rambokParams:
      tools.utils.remove_redundant_params("--unassigned2", rambokParams)
      
   if "-n" in rambokParams:
      tools.utils.remove_redundant_params("-n", rambokParams)
       
   if "--name1" in rambokParams:
      tools.utils.remove_redundant_params("--name1", rambokParams)
   
   if "-t" in rambokParams:
      tools.utils.remove_redundant_params("-t", rambokParams)
   
   if "--threads" in rambokParams:
      tools.utils.remove_redundant_params("--threads", rambokParams)
      
   #----------------------------------------------------------------------------------------
  
   
   pythonPath = tools.utils.parse_config(name = "python2")
   runPath = tools.utils.parse_config(name = "rambok") 
   rambokVersion = "1.2" # read automatically from tool output: not yet available

   simReads = "50000" # default parameter of RAMBO-K
      
   if "-a" in rambokParams: 
      simReads = rambokParams[rambokParams.index("-a")+1]
   if "--amount" in rambokParams:
      simReads = rambokParams[rambokParams.index("--amount")+1]
   
   speciesFg = speciesFgName 
   speciesBg = "species2" # default parameter of RAMBO-K
          
   if "-N" in rambokParams:
      speciesBg = rambokParams[rambokParams.index("-N")+1]
   if "--name2" in rambokParams:
      speciesBg = rambokParams[rambokParams.index("--name2")+1]
     
   kmer = "8" # default parameter of RAMBO-K
   if evaluate: 
      kmer = "4:12"
   if "-k" in rambokParams:
      kmer = rambokParams[rambokParams.index("-k")+1]
   if "--kmersizes" in rambokParams:
      kmer = rambokParams[rambokParams.index("--kmersizes")+1]
   
   kSizes = ["8"]
   if ":" in kmer:
      kSizes = [str(k) for k in range(int(kmer.split(":")[0]), int(kmer.split(":")[1])+1)]
   elif "," in kmer:
      kSizes = [k for k in kmer.split(",")]
   else:
      kSizes = [kmer]
      
   if not "-k" in rambokParams and not "--kmersizes" in rambokParams:
      rambokParams.extend(["-k", ",".join(kSizes)])

   if evaluate:
      if "-C" in rambokParams:
         del rambokParams[rambokParams.index("-C")+1]
         del rambokParams[rambokParams.index("-C")]
         warnings.warn("RAMBO-K was called in evaluation mode. Cutoff value -C will be ignored.")
      if "--cutoff_higher" in rambokParams:
         del rambokParams[rambokParams.index("--cutoff_higher")+1]
         del rambokParams[rambokParams.index("--cutoff_higher")]
         warnings.warn("RAMBO-K was called in evaluation mode. Cutoff value --cutoff_higher will be ignored.")
      if "-c" in rambokParams:
         del rambokParams[rambokParams.index("-c")+1]
         del rambokParams[rambokParams.index("-c")]
         warnings.warn("RAMBO-K was called in evaluation mode. Cutoff value -c will be ignored.")
      if "--cutoff_lower" in rambokParams:
         del rambokParams[rambokParams.index("--cutoff_lower")+1]
         del rambokParams[rambokParams.index("--cutoff_lower")]
         warnings.warn("RAMBO-K was called in evaluation mode. Cutoff value -cutoff_lower will be ignored.")
      
   else:
      if "-C" in rambokParams:
         cutoff = rambokParams[rambokParams.index("-C")+1]
      elif "--cutoff_higher" in rambokParams:
         cutoff = rambokParams[rambokParams.index("--cutoff_higher")+1]
      elif "-c" in rambokParams:
         cutoff = rambokParams[rambokParams.index("-c")+1]
         warnings.warn("Selected cutoff parameter -c would lead to extraction of background reads, setting value for parameter -C!")
      elif "--cutoff_lower" in rambokParams:
         cutoff = rambokParams[rambokParams.index("--cutoff_lower")+1]
         warnings.warn("Selected cutoff parameter --cutoff_lower would lead to extraction of background reads, setting value for parameter --cutoff_higher!")
      else:
         cutoff = "0"
         rambokParams.extend(["-C","0"])
        
   reportInfo = {"toolName" : reportName,
                 "rambok_version" : rambokVersion,
                 "sampleName" : sampleName,
                 "speciesFg" : speciesFg,
                 "speciesBg" : speciesBg,
                 "refFg" : refFg,
                 "refBg" : refBg,
                 "kSizes" : ','.join(str(k) for k in kSizes)}

   rambokArgs = [pythonPath, runPath]
  
   if pairedInputFiles:
      
      rambokArgs.extend(["-1", pairedInputFiles[0],
                         "-2", pairedInputFiles[1],
                         "-o", pairedOutputPath,
                         "-r", refFg,
                         "-R", refBg,
                         "-n", speciesFg,
                         "-t", str(threads)])

      rambokArgs.extend(rambokParams)

      log = open(logFile,"a")
      rambok = subprocess.Popen(rambokArgs, stdout=log, stderr=log)
      rambok.communicate()
    

    
      pairedReportInfo = reportInfo.copy()
      callParams = " ".join(rambokArgs)
                        
      if evaluate:
                               
         pairedReportInfo.update({"call" : callParams,
                                  "histogram" : pairedOutputPath + "/graphics/fitted_histograms_kxxx_" + simReads + "_PE.png",
                                  "roc" : pairedOutputPath + "/graphics/ROCplot_kxxx_" + simReads + "_PE.png",
                                  "scores" : pairedOutputPath + "/graphics/score_histogram_kxxx_" + simReads + "_PE.png"})

         tools.utils.export_info(pairedReportInfo, pairedOutputPath + "/kmer_plots_paired_stats.txt")
         tools.utils.generate_latex_report(None, None, [pairedOutputPath + "/kmer_plots_paired_stats.txt"], "RAMBO-K_template", "kmer_plots_paired_eval_" + sampleName + ".pdf", pairedOutputPath)     

      else:
         
         reportInfo.update({"call_paired" : callParams,
                            "histogram_paired" : pairedOutputPath + "/graphics/fitted_histograms_k"+kmer+"_" + simReads + "_PE.png",
                            "roc_paired" : pairedOutputPath + "/graphics/ROCplot_k"+kmer+"_" + simReads + "_PE.png",
                            "scores_paired" : pairedOutputPath + "/graphics/score_histogram_k"+kmer+"_" + simReads + "_PE.png"})
         
         shutil.copy(pairedOutputPath + "/" + speciesFg + "_cutoff_" + cutoff + "_k_" + kmer + "_1.fastq", kwargs["pairedFwOutputFile"])
         shutil.copy(pairedOutputPath + "/" + speciesFg + "_cutoff_" + cutoff + "_k_" + kmer + "_2.fastq", kwargs["pairedRevOutputFile"])
         
   
   rambokArgs = [pythonPath, runPath]
   
   if unpairedInputFile:
      
      rambokArgs.extend(["-1", unpairedInputFile[0],
                         "-o", unpairedOutputPath,
                         "-r", refFg,
                         "-R", refBg,
                         "-n", speciesFg,
                         "-t", str(threads)])

      rambokArgs.extend(rambokParams)

      rambok = subprocess.Popen(rambokArgs, stdout=log, stderr=log)
      rambok.communicate()

      
      unpairedReportInfo = reportInfo.copy()
      
      if evaluate:

         unpairedReportInfo.update({"call" : " ".join(rambokArgs),
                                    "histogram" : unpairedOutputPath + "/graphics/fitted_histograms_kxxx_" + simReads + "_SE.png",
                                    "roc" : unpairedOutputPath + "/graphics/ROCplot_kxxx_" + simReads + "_SE.png",
                                    "scores" : unpairedOutputPath + "/graphics/score_histogram_kxxx_" + simReads + "_SE.png"})
                 

         tools.utils.export_info(unpairedReportInfo, unpairedOutputPath + "/kmer_plots_unpaired_stats.txt")
         tools.utils.generate_latex_report(None, None, [unpairedOutputPath + "/kmer_plots_unpaired_stats.txt"], "RAMBO-K_template", "kmer_plots_unpaired_eval_" + sampleName + ".pdf", unpairedOutputPath )     
      else:
         
         reportInfo.update({"call_unpaired" : " ".join(rambokArgs),
                           "histogram_unpaired" : unpairedOutputPath + "/graphics/fitted_histograms_k"+kmer+"_" + simReads + "_SE.png",
                           "roc_unpaired" : unpairedOutputPath + "/graphics/ROCplot_k"+kmer+"_" + simReads + "_SE.png",
                           "scores_unpaired" : unpairedOutputPath + "/graphics/score_histogram_k"+kmer+"_" + simReads + "_SE.png"})
         
         shutil.copy(unpairedOutputPath + "/" + speciesFg + "_cutoff_" + cutoff + "_k_" + kmer + ".fastq", kwargs["unpairedOutputFile"])

   if evaluate: 

      tools.utils.export_info(reportInfo, reportFile)
      print("Pipeline was run with RAMBO-K in test mode! Please select appropriate kmer size and a cutoff and restart the pipeline with these parameters!\n")
   else:
      
      nrOfPairedInputReads, noOfPairedInputBases = tools.utils.calculate_read_stats(pairedInputFiles)
      nrOfUnpairedInputReads, noOfUnpairedInputBases = tools.utils.calculate_read_stats(unpairedInputFile)
      totalInputReads = nrOfPairedInputReads + nrOfUnpairedInputReads
      #totalInputBases = noOfPairedInputBases + noOfUnpairedInputBases
      
      if pairedInputFiles:
         pairedOutputFiles = [kwargs["pairedFwOutputFile"], kwargs["pairedRevOutputFile"]]
      else:
         pairedOutputFiles = []
      
      if unpairedInputFile:
         unpairedOutputFile = [kwargs["unpairedOutputFile"]]
      else:
         unpairedOutputFile = []
      
      nrOfPairedRambokReads, noOfPairedRambokBases = tools.utils.calculate_read_stats(pairedOutputFiles)
      nrOfUnpairedRambokReads, noOfUnpairedRambokBases = tools.utils.calculate_read_stats(unpairedOutputFile)
      totalRambokReads = nrOfPairedRambokReads + nrOfUnpairedRambokReads
      #totalRambokBases = noOfPairedRambokBases + noOfUnpairedRambokBases
      
      if nrOfPairedInputReads > 0:
         percentPairedRambokReads = nrOfPairedRambokReads/nrOfPairedInputReads*100
      else:
         percentPairedRambokReads = 0
      
      if nrOfUnpairedInputReads > 0:
         percentUnpairedRambokReads = nrOfUnpairedRambokReads/nrOfUnpairedInputReads*100
      else:
         percentUnpairedRambokReads = 0
         
      percentTotalRambokReads = totalRambokReads/totalInputReads*100
      
      reportInfo.update({"nrOfPairedRambokReads" : "{:,d}".format(nrOfPairedRambokReads),
                         "nrOfUnpairedRambokReads" : "{:,d}".format(nrOfUnpairedRambokReads),
                         "totalRambokReads" : "{:,d}".format(totalRambokReads),
                         "percentPairedRambokReads" : "{:.1f}".format(percentPairedRambokReads),
                         "percentUnpairedRambokReads" : "{:.1f}".format(percentUnpairedRambokReads),
                         "percentTotalRambokReads" : "{:.1f}".format(percentTotalRambokReads),
                         "cutoff" : cutoff})

      tools.utils.export_info(reportInfo, reportFile)

def subsample(sampleName, genomeSize, givenCoverage, pairedInputFiles, unpairedInputFile, pairedFwOutputFile, pairedRevOutputFile, unpairedOutputFile, reportFile, reportName):

   print("\n............. Subsampling .............")

   if genomeSize:
      reportInfo = {"toolName" : reportName,
                    "genomeSize" : "{:,d}".format(int(genomeSize)),
                    "givenCoverage" : "{:,d}".format(int(givenCoverage))}
   
      nrOfPairedReads, noOfPairedBases = tools.utils.calculate_read_stats(pairedInputFiles)
      nrOfUnpairedReads, noOfUnpairedBases = tools.utils.calculate_read_stats(unpairedInputFile)
      totalReads = nrOfPairedReads + nrOfUnpairedReads
      totalBases = noOfPairedBases + noOfUnpairedBases
      meanReadLength = totalBases/totalReads
         
      expCov = totalBases/int(genomeSize)
         
      reportInfo["expectedCoverage"] = "{0:,.2f}".format(expCov)
         
      if expCov > float(givenCoverage):
         warnings.warn("The expected coverage is " + str(expCov) + "! This is too high and will be subsampled down to around " + str(givenCoverage) + "x coverage!")
            
         noOfReadsNeeded = numpy.ceil(int(genomeSize) * float(givenCoverage)/meanReadLength)
                
         timestamp = int(datetime.datetime.now().microsecond)
         random.seed(timestamp) 
      
         pairedRecordsToKeep = []
         percentPaired = 0
         unpairedRecordsToKeep = []
         percentUnpaired = 0
      
         if pairedInputFiles:     
            percentPaired = nrOfPairedReads/totalReads
            noOfPairedReadsNeeded = int(round(noOfReadsNeeded * percentPaired)/2)
            pairedRecordsToKeep = set(random.sample(range(int(nrOfPairedReads/2) + 1), noOfPairedReadsNeeded))
            write_subsamples(str(pairedInputFiles[0]), str(pairedFwOutputFile), pairedRecordsToKeep)
            write_subsamples(str(pairedInputFiles[1]), str(pairedRevOutputFile), pairedRecordsToKeep)
             
         if unpairedInputFile:          
            percentUnpaired = nrOfUnpairedReads/totalReads
            noOfUnpairedReadsNeeded = int(round(noOfReadsNeeded * percentUnpaired))
            unpairedRecordsToKeep = set(random.sample(range(nrOfUnpairedReads + 1), noOfUnpairedReadsNeeded))
            write_subsamples(str(unpairedInputFile), str(unpairedOutputFile), unpairedRecordsToKeep)
   
         reportInfo.update({"timestamp" : timestamp,
                            "nrOfSubsampledPairedReads" : "{:,d}".format(len(pairedRecordsToKeep)*2),
                            "nrOfSubsampledUnpairedReads" : "{:,d}".format(len(unpairedRecordsToKeep)),
                            "percentOfSubsampledPairedReads" : "{0:.2f}".format(len(pairedRecordsToKeep)*2/(len(pairedRecordsToKeep)*2+len(unpairedRecordsToKeep))*100),
                            "percentOfSubsampledUnpairedReads" : "{0:.2f}".format(len(unpairedRecordsToKeep)/(len(pairedRecordsToKeep)*2+len(unpairedRecordsToKeep))*100),
                            "nrOfInputPairedReads" : "{:,d}".format(nrOfPairedReads),
                            "nrOfInputUnpairedReads" : "{:,d}".format(nrOfUnpairedReads),
                            "nrOfTotalInputReads" : "{:,d}".format(totalReads),
                            "nrOfTotalSubsampledReads" : "{:,d}".format(len(pairedRecordsToKeep)*2+len(unpairedRecordsToKeep)),
                            "percentOfTotalSubsampledReads" : "{0:.2f}".format((len(pairedRecordsToKeep)*2+len(unpairedRecordsToKeep))/totalReads*100),
                            "percentPaired" : "{0:.2f}".format(percentPaired),
                            "percentUnpaired" : "{0:.2f}".format(percentUnpaired)})
   
      else:
         print("No subsampling performed. Exprected coverages is " + str(expCov))
         reportInfo["no_subsampling"] = "True";
         if pairedInputFiles:
            shutil.copy(str(pairedInputFiles[0]), str(pairedFwOutputFile))
            shutil.copy(str(pairedInputFiles[1]), str(pairedRevOutputFile))
         if unpairedInputFile:
            shutil.copy(str(unpairedInputFile), str(unpairedOutputFile))
   else:
      print("No subsampling performed because of missing genomeSize!")

   tools.utils.export_info(reportInfo, reportFile)

def sam_to_bam(files, threads):
   
   print("Convert sam to bam with samtools ...")   
   
   samtoolsRunPath = tools.utils.parse_config(name = "samtools")
   
   for samFile in files:
      samtoolsArgs = [samtoolsRunPath, "view", "--threads", str(threads),
                  "-bS", samFile]
      bamFileName = samFile.replace("sam","bam")
      with open(bamFileName,"w") as bamFile:
         samToBam = subprocess.Popen(samtoolsArgs, stdout = bamFile)  
         samToBam.communicate()         

         
def sort_bam(files, threads):
   
   print("Sort bam files with samtools ...")     
   
   samtoolsRunPath = tools.utils.parse_config(name = "samtools")
     
   for bamFile in files:
      samtoolsArgs = [samtoolsRunPath, "sort", "--threads", str(threads), bamFile,
                      "-o", bamFile.replace(".bam","_sorted.bam")]
     
      sortBam = subprocess.Popen(samtoolsArgs)  
      sortBam.communicate()    
         
def index_bam(files, threads):
   
   print("Index bam files with samtools ...")
    
   samtoolsRunPath = tools.utils.parse_config(name = "samtools")
              
   for bamFile in files:
      samtoolsArgs = [samtoolsRunPath, "index", "-@", str(threads),
                     bamFile, bamFile.replace("bam","bai")]

      indexBam = subprocess.Popen(samtoolsArgs)
      indexBam.communicate()
      
def merge_bams(files, outFile, threads):
   
   print("Merge bam files with samtools ...")
   
   samtoolsRunPath = tools.utils.parse_config(name = "samtools")
   
   samtoolsArgs = [samtoolsRunPath, "merge", "--threads", str(threads), 
                  outFile]
   samtoolsArgs.extend(files)

   mergeBam = subprocess.Popen(samtoolsArgs)
   mergeBam.communicate()
   
# def cov_stats(inputFile, outputFile):
#    
#    print("Calculating coverage stats with samtools ...")
#    
#    samtoolsRunPath = tools.utils.parse_config(name = "samtools")
# 
#    samtoolsArgs = [samtoolsRunPath, "mpileup",
#                   inputFile,
#                   "-o", outputFile]
#    
#    covStats = subprocess.Popen(samtoolsArgs)
#    covStats.communicate()

def cov_stats(inputFile, reportFile, reportName):
   
   print("Calculating coverage stats with bedtools and plot with R...")
   
   reportInfo = {"toolName" : reportName,
                  "meanCoverage" : "0"}
   
   bedtoolsRunPath = tools.utils.parse_config(name = "bedtools")

   bedtoolsRunPath = [bedtoolsRunPath, "genomecov",
                      "-d", "-ibam", inputFile]

   with open(inputFile.replace(".bam",".genomecov"),"w") as outFile:
      cov_stats = subprocess.Popen(bedtoolsRunPath, stdout = outFile)    
      cov_stats.communicate()
          
   
   rscriptRunPath = tools.utils.parse_config(name = "rscript")
   
   plotScript = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "plot_coverage.R")
   
   rscriptArgs = [rscriptRunPath, plotScript,
                  inputFile.replace(".bam",".genomecov"), inputFile.replace(".bam",".png")]

   plot = subprocess.Popen(rscriptArgs)
   plot.communicate()
      
   reportInfo["cov_plot"] = inputFile.replace(".bam",".png")   
      
   tools.utils.export_info(reportInfo, reportFile)


def nucmer(refFile, queryFile, nameBase, nucmerParams, reportFile, reportName):
   
   if refFile:
      print("Plotting nucmer plots ...")
      
      
      #----------------------------------------------------------------------------------------
      #
      # predefined parameters:
      # -p/-prefix
      # 
      
      if "-p" in nucmerParams:
         tools.utils.remove_redundant_params("-p", nucmerParams)
      
      if "-prefix" in nucmerParams:
         tools.utils.remove_redundant_params("-prefix", nucmerParams)
      
      #----------------------------------------------------------------------------------------
            
      nucmerRunPath = tools.utils.parse_config(name = "nucmer")
      
      nucVers = str.splitlines(subprocess.Popen([nucmerRunPath, "--version"], stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])[1].replace("NUCmer (NUCleotide MUMmer) version ","")
         
   
      reportInfo = {"toolName" : reportName,
                    "nucmer_version" : nucVers}
      
      nucmerArgs = [nucmerRunPath, 
                    "-p", nameBase,
                    refFile, queryFile]
      nucmer = subprocess.Popen(nucmerArgs)
      nucmer.communicate()
      
      reportInfo["call"] = " ".join(nucmerArgs)
      
      
#       tmp = ""
#       
#       with open(nameBase + "_tmp.delta", "r") as f:
#          with open(nameBase + ".delta", "w") as out:
#             for line in f:         
#                if line.startswith(">"):
#                   tmp = line.rstrip()
#                else: 
#                   out.write(tmp + line)
#                   tmp=""
               
      mummerplotRunPath = tools.utils.parse_config(name = "mummerplot")
      mummerplotVers = str.splitlines(subprocess.Popen([mummerplotRunPath, "--version"], stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])[-1:][0].replace("mummerplot ","")
      
      reportInfo["mummerplotVers"] = mummerplotVers
      
      mummerplotArgs = [mummerplotRunPath,
                        "--png",
                        "-p", nameBase,
                        nameBase + ".delta"]
      mummerplot = subprocess.Popen(mummerplotArgs)
      mummerplot.communicate()
   
      reportInfo["mummerplot_call"] = " ".join(mummerplotArgs)
      if os.path.exists(nameBase + ".png"):
         reportInfo["plotPath"] = nameBase + ".png"
      
      tools.utils.export_info(reportInfo, reportFile)
      
   else:
      print("No nucmer plot was created due to missing reference sequence!")
              
def write_subsamples(inputFile, outputFile, readsToKeep):
   
   recordNo = 0
     
   with open(inputFile) as allReads:
      with open(outputFile, "w") as sampledReads:
         for line1 in allReads:
            line2 = next(allReads)
            line3 = next(allReads)
            line4 = next(allReads)
            if recordNo in readsToKeep:
               sampledReads.write(line1)
               sampledReads.write(line2)
               sampledReads.write(line3)
               sampledReads.write(line4)
            recordNo += 1
