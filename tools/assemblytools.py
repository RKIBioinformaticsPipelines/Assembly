import tools, subprocess, shutil, os, warnings

def spades(sampleName, pairedInputFiles, unpairedInputFile, spadesParams, genomeSize, outputPath, contigPath, reportFile, reportName, threads):
   
   """ Run de nov assembly using spades. """

   print("\n............. Starting SPAdes ............") 
       
   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # -o
   # -1
   # -2
   # -s
   #
   
   if "-o" in spadesParams:
      tools.utils.remove_redundant_params("-o", spadesParams)
   
   if "-1" in spadesParams:
      tools.utils.remove_redundant_params("-1", spadesParams)
 
   if "-2" in spadesParams:
      tools.utils.remove_redundant_params("-2", spadesParams)
     
   if "-s" in spadesParams:
      tools.utils.remove_redundant_params("-s", spadesParams)
   
   if "-t" in spadesParams:
      tools.utils.remove_redundant_params("-t", spadesParams)
      
   if "--threads" in spadesParams:
      tools.utils.remove_redundant_params("--threads", spadesParams)
  
      
   #----------------------------------------------------------------------------------------
           
   runPath = tools.utils.parse_config(name = "spades") 
   spadesVers = str.splitlines(subprocess.Popen(runPath, stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])[0].replace("SPAdes genome assembler v.","")
      
   reportInfo = {"toolName" : reportName,
                 "spades_version" : spadesVers,
                 "sampleName" : sampleName}
  
      
   spadesArgs = [runPath,
                  "-o", outputPath,
                  "-t", str(threads)]
   
   spadesArgs.extend(spadesParams)

   if pairedInputFiles:
      spadesArgs.extend(["-1", pairedInputFiles[0]])
      spadesArgs.extend(["-2", pairedInputFiles[1]])
   
   if unpairedInputFile:
      spadesArgs.extend(["-s"] + unpairedInputFile)

   spades = subprocess.Popen(spadesArgs)  
   spades.communicate() 
   
   reportInfo["call"] = " ".join(spadesArgs)
   

   shutil.copy(outputPath + "/contigs.fasta", contigPath)  
   
   contigLengths = []

   with open(contigPath, "r") as f:

      for line in f:
         if line.startswith(">"):
            seqInfo = line.split("_")
            contigLengths.append(int(seqInfo[3]))
                
   contigLengthsSorted = sorted(contigLengths, reverse = True)
   n50Length = sum(contigLengths) / 2
  
   contigLengthsSum = 0
   index = 0
         
   n50 = 0
   l50 = 0
            
   for l in contigLengthsSorted:
      contigLengthsSum += l
            
      if(contigLengthsSum >= n50Length):
         n50 = contigLengthsSorted[index]
         l50 = index + 1
         break
      index += 1
      
   maxL = 0
   if len(contigLengths) > 0:
      maxL = max(contigLengths)
   reportInfo.update({"N50" : "{:,d}".format(n50),
                      "L50"  : "{:,d}".format(l50),
                      "nrOfContigs" : "{:,d}".format(len(contigLengths)),
                      "maxContigLength" : "{:,d}".format(maxL),
                      "totalLength" : "{:,d}".format(sum(contigLengths))})       


   if genomeSize:
      reportInfo.update({"totalBasesPercent" : "{:.1f}".format(sum(contigLengths)/int(genomeSize)),
                         "maxContigLengthPercent" : "{:.1f}".format(maxL/int(genomeSize))})

      
   tools.utils.export_info(reportInfo, reportFile)
   
def afive(sampleName, pairedInputFiles, afiveParams, genomeSize, outputPath, contigPath, reportFile, reportName, threads):
   
   print("\n............. Starting a5 ............")

   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # --threads

   if "--threads" in afiveParams:
      tools.utils.remove_redundant_params("--threads=", afiveParams)


   runPath = tools.utils.parse_config(name = "A5")   
   a5VersTmp = str.splitlines(subprocess.Popen(runPath, stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])
   a5Vers = [i for i in a5VersTmp if "A5-miseq version" in i][0]

   reportInfo = {"toolName" : reportName,
                 "a5_version" : a5Vers,
                 "sampleName" : sampleName}
   
   libcontent = "[LIB]\n"
   

   pairedFwFileName = ""
   pairedRevFileName = ""
   #unpairedFileName = ""
   
   if pairedInputFiles:
  
      pairedFwFileName = str(pairedInputFiles[0]).split("/")[-1]
      pairedRevFileName = str(pairedInputFiles[1]).split("/")[-1]
      shutil.copy(pairedInputFiles[0], outputPath + "/" + pairedFwFileName)
      shutil.copy(pairedInputFiles[1], outputPath + "/" + pairedRevFileName)
      libcontent = libcontent + "p1=" + pairedFwFileName + "\n"
      libcontent = libcontent + "p2=" + pairedRevFileName + "\n"
   
   #if unpairedInputFile:
   #   unpairedFileName = str(unpairedInputFile[0]).split("/")[-1]
   #   shutil.copy(unpairedInputFile[0], outputPath + "/" + unpairedFileName)   
   #   libcontent = libcontent + "up=" + unpairedFileName + "\n"
   
   with open(outputPath + "library", "w") as lib:
         lib.write(libcontent)
         
 
   afiveArgs = [runPath, "--threads=" + str(threads), "library", "a5"]
   afiveArgs.extend(afiveParams)

   afive = subprocess.Popen(afiveArgs, cwd = outputPath)  
   afive.communicate() 
   
   reportInfo["call"] = " ".join(afiveArgs)
      
   shutil.copy(outputPath + "/a5.contigs.fasta", contigPath)
   
   if pairedInputFiles:
      os.remove(outputPath + "/" + pairedFwFileName)
      os.remove(outputPath + "/" + pairedRevFileName)
   #if unpairedInputFile:
   #   os.remove(outputPath + "/" + unpairedFileName)
      
   contigLengths = []

   with open(contigPath, "r") as f:
 
      seqLen = 0
      nrSeqsFound = 0
      for line in f:
         if line.startswith(">"):
            if nrSeqsFound > 0:
               contigLengths.append(seqLen)
            else:
               nrSeqsFound += 1
            seqLen = 0
         else:
            seqLen = seqLen + len(line.rstrip())
      
      contigLengths.append(seqLen)
             
   contigLengthsSorted = sorted(contigLengths, reverse = True)
   n50Length = sum(contigLengths) / 2
   
   contigLengthsSum = 0
   index = 0
          
   n50 = 0
   l50 = 0
             
   for l in contigLengthsSorted:
      contigLengthsSum += l
             
      if(contigLengthsSum >= n50Length):
         n50 = contigLengthsSorted[index]
         l50 = index + 1
         break
      index += 1

   maxL = 0
   if len(contigLengths) > 0:
      maxL = max(contigLengths)
   reportInfo.update({"N50" : "{:,d}".format(n50),
                      "L50"  : "{:,d}".format(l50),
                      "nrOfContigs" : "{:,d}".format(len(contigLengths)),
                      "maxContigLength" : "{:,d}".format(maxL),
                      "totalLength" : "{:,d}".format(sum(contigLengths))})       
 
 
   if genomeSize:
      reportInfo.update({"totalBasesPercent" : "{:.1f}".format(sum(contigLengths)/int(genomeSize)),
                         "maxContigLengthPercent" : "{:.1f}".format(maxL/int(genomeSize))})

   
   tools.utils.export_info(reportInfo, reportFile)

def velvet(sampleName, pairedInputFiles, unpairedInputFile, hashsToTest, velvetOptParams, velvethParams, velvetgParams, genomeSize, outputPath, contigPath, reportFile, reportName, threads):
   
   optRunPath = tools.utils.parse_config(name = "velvetopt")   
   velvetoptVers = subprocess.Popen([optRunPath, "--version"], stderr = subprocess.PIPE, stdout = subprocess.PIPE, universal_newlines = True).communicate()[0].replace("VelvetOptimiser ","").rstrip()
   

   reportInfo = {"toolName" : reportName,
                 "velvetOptimiserVersion" : velvetoptVers,
                 "sampleName" : sampleName}
   
   print("\n............. Starting VelvetOptimiser ............")

   #----------------------------------------------------------------------------------------
   #
   # predefined parameters for velveth:
   # -fasta
   # -fastq
   # -raw
   # -fasta.gz
   # -raw.gz
   # -sam
   # -bam
   # -fmtAuto
   # -shortPaired
   # -short  
   
   if "-fasta" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -fasta will be ignored.")
      tools.utils.remove_redundant_params("-fasta", velvethParams)
      
   if "-fastq" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -fastq will be ignored.")
      tools.utils.remove_redundant_params("-fastq", velvethParams)
   
   if "-raw" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -raw will be ignored.")
      tools.utils.remove_redundant_params("-raw", velvethParams)
   
   if "-fasta.gz" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -fasta.gz will be ignored.")
      tools.utils.remove_redundant_params("-fasta.gz", velvethParams)
      
   if "-raw.gz" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -raw.gz will be ignored.")
      tools.utils.remove_redundant_params("-raw.gz", velvethParams)
      
   if "-sam" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -sam will be ignored.")
      tools.utils.remove_redundant_params("-sam", velvethParams)
   
   if "-bam" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -bam will be ignored.")
      tools.utils.remove_redundant_params("-bam", velvethParams)
   
   if "-fmtAuto" in velvethParams:
      warnings.warn("Input file format already specified as -fastq. Parameter setting for -fmtAuto will be ignored.")
      tools.utils.remove_redundant_params("-fmtAuto", velvethParams)
      
   if "-shortPaired" in velvethParams:
      warnings.warn("Input files already specified for -shortPaired. Parameter setting for -shortPaired will be ignored.")
      tools.utils.remove_redundant_params("-shortPaired", velvethParams)
      
   if "-short" in velvethParams:
      warnings.warn("Input files already specified for -short. Parameter setting for -short will be ignored.")
      tools.utils.remove_redundant_params("-short", velvethParams)

   velvethArgs = ["-fastq"]
   
   if pairedInputFiles: 
    
         velvethArgs.extend(["-shortPaired", "-separate"] + pairedInputFiles)
   
   if unpairedInputFile:
         
         velvethArgs.extend(["-short"] + unpairedInputFile)
         
   velvethArgs.extend(velvethParams)  
   velvethfiles = "'" + " ".join(velvethArgs) + "'" 
    
   
   #----------------------------------------------------------------------------------------
      #
      # predefined parameters:
      # --s/--hashs
      # --e/--hashe
      # --x/--step
      # --f/--velvethfiles
      # --d/--dir_final
      # --o/--velvetgoptions
      # --p/--prefix
      #
   
   if "--s" in velvetOptParams:
      tools.utils.remove_redundant_params("--s", velvetOptParams)
   
   if "--hash" in velvetOptParams:
      tools.utils.remove_redundant_params("--hash", velvetOptParams)   
   
   if "--e" in velvetOptParams:
      tools.utils.remove_redundant_params("--e", velvetOptParams)   
   
   if "--hashe" in velvetOptParams:
      tools.utils.remove_redundant_params("--hashe", velvetOptParams)
      
   if "--x" in velvetOptParams:
      tools.utils.remove_redundant_params("--x", velvetOptParams)

   if "--step" in velvetOptParams:
      tools.utils.remove_redundant_params("--step", velvetOptParams)

   if "--f" in velvetOptParams:
      tools.utils.remove_redundant_params("--f", velvetOptParams)

   if "--velvethfiles" in velvetOptParams:
      tools.utils.remove_redundant_params("--velvethfiles", velvetOptParams)
      
   if "--d" in velvetOptParams:
      tools.utils.remove_redundant_params("--d", velvetOptParams)

   if "--dir_final" in velvetOptParams:
      tools.utils.remove_redundant_params("--dir_final", velvetOptParams)

   if "--o" in velvetOptParams:
      tools.utils.remove_redundant_params("--o", velvetOptParams)

   if "--velvetgoptions" in velvetOptParams:
      tools.utils.remove_redundant_params("--velvetgoptions", velvetOptParams)
      
   if "--p" in velvetOptParams:
      tools.utils.remove_redundant_params("--p", velvetOptParams)
   
   if "--prefix" in velvetOptParams:
      tools.utils.remove_redundant_params("--prefix=", velvetOptParams)

   if "--t" in velvetOptParams:
      tools.utils.remove_redundant_params("--t", velvetOptParams)
   
   if "--threads" in velvetOptParams:
      tools.utils.remove_redundant_params("--threads=", velvetOptParams)

   if hashsToTest:
   
      hashs = hashsToTest.split(",")
      velvetOptParams.extend(["--s", hashs[0],
                              "--e", hashs[1],
                              "--x", hash[2],
                              "--t", str(threads)])
         
   else:
      
      print("Hash values for velvetOptimizer not given. Calculate values from read lengths.")
      
      nrOfPairedReads, noOfPairedBases = tools.utils.calculate_read_stats(pairedInputFiles)
      nrOfUnpairedReads, noOfUnpairedBases = tools.utils.calculate_read_stats(unpairedInputFile)
      totalReads = nrOfPairedReads + nrOfUnpairedReads
      totalBases = noOfPairedBases + noOfUnpairedBases
      meanReadLength = totalBases/totalReads
      
      velvetOptParams.extend(["--s", str(int(0.2*meanReadLength)), # max read length instead of mean read length?
                              "--e", str(int(0.8*meanReadLength)),
                              "--x", "2",
                              "--t", str(threads)])
         
   
   velvetOptArgs = [optRunPath,
                    "--d", outputPath + "/optimized/",
                    "--f", velvethfiles,
                    "--p", "velvet_tmp_" + sampleName.replace("-","_")]
   velvetOptArgs.extend(velvetOptParams)
   
   if velvetgParams:
      velvetOptArgs.extend(["--o",  "'" + " ".join(velvetgParams) + "'"])
   
   
   velvetOpt = subprocess.Popen(" ".join(velvetOptArgs),  shell=True)
   velvetOpt.communicate()

   reportInfo["call"] = " ".join(velvetOptArgs)

   shutil.copy(outputPath + "/optimized/contigs.fa", contigPath) 

  
   resultlog = ""
   with open(outputPath + "/optimized/velvet_tmp_" + sampleName.replace("-","_") + "_logfile.txt", "r") as f:
      
      skip = False   
      for line in f:

         if skip:
            resultlog = resultlog + line
            
         if "Final optimised assembly details:" in line:

            skip = True
  

      
   for val in resultlog.split("\n"):
   
      if "Velveth version: " in val:
         velvethVers = val.replace("Velveth version: ", "").rstrip()
      if "Velvetg version: " in val:
         velvetgVers = val.replace("Velvetg version: ", "").rstrip()
      if "Velveth parameter string: " in val:
         velvethParamString = val.replace("Velveth parameter string: ", "").rstrip()
      if "Velvetg parameter string: " in val:
         velvetgParamString = val.replace("Velvetg parameter string: ", "").rstrip()
      if "Velvet hash value: " in val:
         hashVal = val.replace("Velvet hash value: ", "").strip()
     
   reportInfo.update({"velvethVersion" : velvethVers,
                      "velvetgVersion"  : velvetgVers,
                      "velvethParams" : velvethParamString,
                      "velvetgParams" : velvetgParamString})       

   contigLengths = []
   coverages = []
   
   with open(outputPath + "/optimized/stats.txt", "r") as stats:
      next(stats) # skip header
      
      for line in stats:
               
         col = line.strip().split("\t")
         if int(col[1]) == 0: # removes lines with contig length 0 from stats.txt file !!!
            continue
         contigLengths.append(int(col[1]) + int(hashVal) - 1)
         coverages.append(float(col[4]) + float(col[5]) + float(col[7]))

         
   contigLengthsSorted = sorted(contigLengths, reverse = True)
   n50Length = sum(contigLengths) / 2
   

   contigLengthsSum = 0
   pos = 0       
   n50 = 0
   l50 = 0
           
   for l in contigLengthsSorted:
      contigLengthsSum += l
            
      if(contigLengthsSum >= n50Length):
         n50 = contigLengthsSorted[pos]
         l50 = pos + 1
         break
      pos += 1        
 
   maxL = 0
   if len(contigLengths) > 0:
      maxL = max(contigLengths)
   reportInfo.update({"N50" : "{:,d}".format(n50),
                      "L50"  : "{:,d}".format(l50),
                      "nrOfContigs" : "{:,d}".format(len(contigLengths)),
                      "maxContigLength" : "{:,d}".format(maxL),
                      "totalLength" : "{:,d}".format(sum(contigLengths))})       


   if genomeSize:
      reportInfo.update({"totalBasesPercent" : "{:.1f}".format(sum(contigLengths)/int(genomeSize)),
                         "maxContigLengthPercent" : "{:.1f}".format(maxL/int(genomeSize))})

   tools.utils.export_info(reportInfo, reportFile)
