import tools, subprocess, warnings

def bowtie2_index(sampleName, reference, indexBase, threads):
   
   bowtieBuildRunPath = tools.utils.parse_config(name = "bowtie2-build")
   bowtie2BuildArgs = [bowtieBuildRunPath, "--threads", str(threads), reference,
                       indexBase]

   bowtie2Build = subprocess.Popen(bowtie2BuildArgs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   bowtie2Build.communicate()
   

def bowtie2(sampleName, pairedInputFiles, unpairedInputFile, pairedsam, unpairedsam, indexBase, bowtie2Params, outputPath, reportFile, reportName, threads):
   
   print("\n------> Generating bowtie2 index --------")  
   
   #----------------------------------------------------------------------------------------
   #
   # predefined parameters:
   # -x
   # -1
   # -2
   # -U
   # -S
   # -p / --threads
 
   if "-x" in bowtie2Params:
      tools.utils.remove_redundant_params("-x", bowtie2Params)

   
   if "-S" in bowtie2Params:
      tools.utils.remove_redundant_params("-S", bowtie2Params)
      
   if "-1" in bowtie2Params:
      tools.utils.remove_redundant_params("-1", bowtie2Params)  
   
   if "-2" in bowtie2Params:
      tools.utils.remove_redundant_params("-2", bowtie2Params)
      
   if "-U" in bowtie2Params:
      tools.utils.remove_redundant_params("-U", bowtie2Params)
   
   if "-p" in bowtie2Params:
      tools.utils.remove_redundant_params("-p", bowtie2Params)
   
   if "--threads" in bowtie2Params:
      tools.utils.remove_redundant_params("--threads", bowtie2Params)
   
   #----------------------------------------------------------------------------------------

   bowtieRunPath = tools.utils.parse_config(name = "bowtie2")
   
   bowtie2_vers = str.splitlines(subprocess.Popen(bowtieRunPath, stderr = subprocess.PIPE, universal_newlines = True).communicate()[1])[1].replace("Bowtie 2 version ","").replace(" by Ben Langmead (langmea@cs.jhu.edu, www.cs.jhu.edu/~langmea)","")
   
   reportInfo = {"toolName" : reportName,
                 "bowtie_version" : bowtie2_vers,
                 "sampleName" : sampleName}

   if pairedInputFiles:
       
      print("\n------> Mapping paired reads with bowtie2 --------")
 
      bowtie2Args = [bowtieRunPath ,
                     "-x", indexBase,
                     "-p", str(threads),
                     "-S", pairedsam,
                     "-1", pairedInputFiles[0],
                     "-2", pairedInputFiles[1]]  
      bowtie2Args.extend(bowtie2Params)
 
      bowtie2 = subprocess.Popen(bowtie2Args, stderr = subprocess.PIPE)    
      out = bowtie2.communicate()
  
      mapRes = str(out[1].decode("utf-8"))

  
      with open(outputPath+"/mapping_stats_paired.txt","w") as outFile:
         outFile.write(mapRes)
      
      pairedMappingRate = "-"
      mappedReadCount = 0
      for l in mapRes.split("\n"):

         if " overall alignment rate" in l:
            pairedMappingRate = l.replace(" overall alignment rate","")
         if " aligned concordantly exactly 1 time" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ","")) * 2
         
         if " aligned concordantly >1 times" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ","")) * 2
                      
         if " aligned discordantly 1 time" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ","")) * 2
         
         if " aligned exactly 1 time" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ",""))
            
         if " aligned >1 times" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ",""))
      
      reportInfo["pairedMappingRate"] = pairedMappingRate      
      reportInfo["numberPairedReadsMapped"] = "{:,d}".format(mappedReadCount)
     
   if unpairedInputFile:
        
      print("\n------> Mapping unpaired reads with bowtie2 --------")
          
      bowtie2Args = [bowtieRunPath ,
                     "-x", indexBase,
                     "-p", str(threads),
                     "-S", unpairedsam,
                     "-U", unpairedInputFile[0]]
      bowtie2Args.extend(bowtie2Params) 
        
      bowtie2 = subprocess.Popen(bowtie2Args,  stderr = subprocess.PIPE)    
      out = bowtie2.communicate()
      
      mapRes = str(out[1].decode("utf-8"))
      
      with open(outputPath+"/mapping_stats_unpaired.txt","w") as outFile:
         outFile.write(mapRes)
      
      unpairedMappingRate = "-"
      mappedReadCount = 0
      for l in mapRes.split("\n"):
         if " overall alignment rate" in l:
            unpairedMappingRate = l.replace(" overall alignment rate","")
         if " aligned exactly 1 time" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ","")) * 2
         if " aligned >1 times" in l:
            mappedReadCount += int(l.split("(")[0].replace(" ","")) * 2
            
      reportInfo["unpairedMappingRate"] = unpairedMappingRate
      reportInfo["numberUnPairedReadsMapped"] = "{:,d}".format(mappedReadCount)

    
   tools.utils.export_info(reportInfo, reportFile)
 