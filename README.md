# Assembly

Version: 0.0.2  
Contact: BI-Support@rki.de  
05.05.2017

## Introduction
The assembly pipeline provides a complete workflow for de novo assembly of NGS reads. It generates contigs using different assembly algorithms which can be selected by the user. Furthermore, assembled contigs are corrected for misassemblies and analyzed vor variants. Assessment and comparison of all assembly results is conducted and results are summarized in a pdf report.  
Additionally, the pipeline provides an optional step that filters background reads before starting the assembly.  
The assembly pipeline can handle multiple samples in parallel (batch assembly).  

The workflow used in the pipeline is visualized in the following chart:


![Workflow](doc/Flowchart_Assembly_Pipeline_reduced.png "Workflow image")


The pipeline uses the following tools:

| Tool name | Version | Link | Pubmed ID | 
|-----------|---------|------|-----------|
|VelvetOptimiser | 2.2.5 | [Homepage](http://www.vicbioinformatics.com/software.velvetoptimiser.shtml) | |
|Velvet | 1.2.10 | [Homepage](https://www.ebi.ac.uk/~zerbino/velvet/) | [18349386](https://www.ncbi.nlm.nih.gov/pubmed/18349386) |
|SPAdes | 3.5.0 | [Homepage](http://cab.spbu.ru/software/spades/) | [22506599](https://www.ncbi.nlm.nih.gov/pubmed/22506599) |
|A5 | 20150522 | [Homepage](https://sourceforge.net/projects/ngopt/) | [25338718](https://www.ncbi.nlm.nih.gov/pubmed/25338718)|
|Bowtie2   | 2.2.4   | [Homepage](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) | [22388286](https://www.ncbi.nlm.nih.gov/pubmed/22388286) |
|SAMtools | 1.3.1 | [Homepage](http://www.htslib.org/)| [19505943](https://www.ncbi.nlm.nih.gov/pubmed/19505943)|
|Pilon  | 1.2.1 | [Homepage](https://github.com/broadinstitute/pilon/wiki) | [25409509](https://www.ncbi.nlm.nih.gov/pubmed/25409509) |
|QUAST | 4.4 | [Homepage](http://quast.sourceforge.net/) | [23422339](https://www.ncbi.nlm.nih.gov/pubmed/23422339)|
|MUMmer | 3.5| [Homepage](http://mummer.sourceforge.net/)| [14759262](https://www.ncbi.nlm.nih.gov/pubmed/14759262) |
|BEDtools | 2.21.0 | [Homepage](http://bedtools.readthedocs.io/en/latest/) | [20110278](https://www.ncbi.nlm.nih.gov/pubmed/20110278) |
|RAMBO-K | 1.2 | | [26379285](https://www.ncbi.nlm.nih.gov/pubmed/26379285)|
